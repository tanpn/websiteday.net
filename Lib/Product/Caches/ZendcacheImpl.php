<?php

class Product_Caches_ZendcacheImpl implements Product_Caches_Interface {
    const CACHE_KEY = 'pro_%s';

    static public $_instance = NULL;
    protected $_zendcache = NULL;

    protected function __construct() {
        $frontendOptions = array(
            'lifetime' => CACHE_EXPIRE,
            'automatic_serialization' => CACHE_SERIALIZE
        );

        $backendOptions = array(
            'cache_dir' => CACHE_DIR // Directory where to put the cache files
        );
        // getting a Zend_Cache_Core object
        $this->_zendcache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    }

    static public function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    //Get cache all items
    public function getCache($id) {
        $key = self::getCacheKey($id);
        $result = $this->_zendcache->load($key); 
        return $result;
    }

    public function setCache($id, $value) {
        $key = self::getCacheKey($id);
        $result = $this->_zendcache->save($value, $key);
        return $result;
    }

    public function removeCache($id) {
        $key = self::getCacheKey($id);
        $result = $this->_zendcache->remove($key);
        return $result;
    }

    protected function getCacheKey($id) {
        return sprintf(self::CACHE_KEY, $id);
    }

}

?>