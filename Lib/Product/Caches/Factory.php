<?php

class Product_Caches_Factory {

    public static function factory($type = 'zendcache') {
        if ($type == 'zendcache') {
            return Product_Caches_ZendcacheImpl::getInstance();
        }else {
            return NULL;
        }
    }

}

?>