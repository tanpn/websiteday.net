<?php

class Product_Storages_InformationImpl implements Product_Storages_Interface {

    static protected $_instance = NULL;
    static protected $_information = "information";
    static protected $_information_description = "information_description";
    
    static public function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function _getDbConnection() {
        return Globals::getDbConnection(Zend_Registry::get('configuration')->db->instance, false);
    }

    public function addInformation($data) {
        try {

            // Init db connection
            $db = $this->_getDbConnection();
            
            // Init variable
            $last_information_id =  0; 

            // Insert base information product
            $information = array(
                'bottom' => $data->getParam("bottom", 0),
                'sort_order' => (int)$data->getParam("sort_order", 0),
                'status' => (int)$data->getParam("status", 0)
            );// var_dump("<pre>",$information,$num_row_inserted);exit;
        
            // Insert product description
            $num_row_inserted =  $db->insert(Product_Storages_InformationImpl::$_information, $information);
            if($num_row_inserted > 0) {
                $sql = 'SELECT max(information_id) as max_id FROM ' .Product_Storages_InformationImpl::$_information; 
                $result =  $db->fetchAll($sql);
                $last_information_id =  $result[0]['max_id']; 
                
                // Get production description
                $arr_product_description =$data->getParam("information_description", null);//var_dump("<pre>",$last_information_id,$arr_product_description);exit;
                if(isset($arr_product_description)) {
                    // Get first item data production
                    $product_description = $arr_product_description[2];//var_dump("<pre>",$arr_product_description,$product_description);exit;

                    $information_description = array(
                          'information_id' => $last_information_id,
                          'language_id' => 1,
                          'title' => $product_description['title'],
                          'description' => $product_description['description'],
                          'meta_title' => $product_description['meta_title'],
                          'meta_description' => $product_description['meta_description'],
                          'meta_keyword' => $product_description['meta_keyword']
                    );//var_dump("<pre>",$product_description);exit;
                    $db->insert(Product_Storages_InformationImpl::$_information_description, $information_description);
                    
                }
            }
        } catch (Exception $ex) {
            throw $ex;
        }
        return 0;
    }

    public function getInformations($data) {
        // Init db connection
        $db = $this->_getDbConnection();

        $sql = "SELECT * from " . Product_Storages_InformationImpl::$_information . " i LEFT JOIN ".Product_Storages_InformationImpl::$_information_description." id ON (i.information_id = id.information_id)";
        $sql .= " WHERE i.information_id = " . $data->getParam("information_id", 0) ." AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC";
        //var_dump("<pre>",$sql);exit;
        return  $db->fetchAll($sql);
	}

    //----------- OLD code ----------------
    private function insertBannerImage($banner_id, $data) {
        //$banner_id = $data->getParam("banner_id", 0);
        //var_dump("<pre>",$banner_id);exit;
        $banner_image = $data->getParam("banner_image", null);
        if($banner_id > 0 && isset($banner_image)) {
            // Init db connection
            $db = $this->_getDbConnection();

            foreach ($banner_image as $item_banner_image) {
				foreach ($item_banner_image as $item_banner_image_value) {//var_dump("<pre>",$banner_image,$item_banner_image_value);exit;
					 $banner_image_data = array(
                      'banner_id' => (int)$banner_id,
                      'language_id' => 1,
                      'title' => $item_banner_image_value['title'],
                      'link' => $item_banner_image_value['link'],
                      'image' => $item_banner_image_value['image'],
                      'sort_order' => (int)$item_banner_image_value['sort_order']
                    );
                    $db->insert(Product_Storages_BannerImpl::$_banner_image, $banner_image_data);
				}
			}
        }
        return 0;
    }

    public function updateBanner($data) {
        try {

            // Init db connection
            $db = $this->_getDbConnection();//var_dump("<pre>",$db);exit;
            
            // Update banner
            $sql = "UPDATE " . Product_Storages_BannerImpl::$_banner . " SET name ='" . $data->getParam("name", '') . "',";
            $sql = $sql . " status = " . $data->getParam("status", 0);
            $sql = $sql . " WHERE banner_id = " . $data->getParam("banner_id", 0); //var_dump("<pre>",$sql);exit;
            $number_banner_update = $db->fetchAll($sql);//var_dump("<pre>",$sql,$number_banner_update);exit;
            
            // Update banner image
            //if($number_banner_update > 0) 
            {
                // Step 1: Delete all banner image belong to banner id
                $sql = "DELETE FROM " . Product_Storages_BannerImpl::$_banner_image;
                $sql = $sql . " WHERE banner_id = " . $data->getParam("banner_id", 0);//var_dump("<pre>",$sql,$number_banner_image_delete);exit;
                $number_banner_image_delete = $db->fetchAll($sql);

                // Step 2: Delete all banner image belong to banner id
                $this->insertBannerImage($data->getParam("banner_id", 0), $data);
            }
        } catch (Exception $ex) {
            throw $ex;
        }
        return 0;
    }

    public function findBanner($data) {
         try {

            // Init db connection
            $db = $this->_getDbConnection();

            // Select sql
            $sql = "select * from " . Product_Storages_BannerImpl::$_banner; 

            // Find banner by banner id
            $banner_id = $data->getParam("banner_id", 0);
            if($banner_id > 0) {
                $sql.= " where banner_id = ". $banner_id;
                
            }
            $sql.= " order by name asc";
            //var_dump($sql);exit;
            return $db->fetchAll($sql);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findBannerImage($data) {
         try {

            // Init db connection
            $db = $this->_getDbConnection();

            // Select sql
            $sql = "select * from " . Product_Storages_BannerImpl::$_banner_image; 

            // Find banner by banner id
            $banner_id = $data->getParam("banner_id", 0);
            if($banner_id > 0) {
                $sql.= " where banner_id = ". $banner_id;
            }
            $sql.= " order by sort_order asc";
            return $db->fetchAll($sql);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

     /**
     * Find list banner image by banner id.
     * @param $cond Condition to filter
     **/
    public function findBannerImageByCondition($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from " . Product_Storages_BannerImpl::$_banner_image ;
            
            if(isset($cond)) {
            	if(isset($cond['banner_id'])) {
            		$sql.= " where banner_id = ". $cond['banner_id'];
            	}
            }
            $sql .= " order by sort_order asc";
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    //------------ Old code -----------
    public function findBanner2($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_BannerImpl::$_banner; 
            if(isset($cond)) {
                if(isset($cond['is_danh_sach'])) {
                    $sql = "select banner_id, name, status from ".Product_Storages_BannerImpl::$_banner;
                    if(isset($cond['status'])) {
            		   $sql.= " where status = ". $cond['status'];
            		}
                }
            	if(isset($cond['banner_id'])) {
            		$sql.= " where banner_id = ". $cond['banner_id'];
            		if(isset($cond['status'])) {
            		    $sql.= " and status = ". $cond['status'];
            		}
            	}
            	//var_dump("<pre>",$cond,$sql);exit;
            }
            $sql .= " order by banner_id desc";
            $result = $db->fetchAll($sql); //var_dump("<pre>",$result,$e);exit;
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getLastProductId() {//var_dump($last_product_id,$data);exit;
    try{
        $sql = 'SELECT max(product_id) as max_id FROM ' .Product_Storages_ProductImpl::$_product; 
        //var_dump($result,$sql,$last_product_id,$data);exit;
         $result =  $db->fetchAll($sql);var_dump($result);exit;
         return $result[0]['max_id']; 
    } catch (Exception $ex) {
        //var_dump($ex);exit;
            throw $ex;
        }
    }

    /**
     * Find product by condition.
     * 
     * @param $cond Condition to filter
     */
    public function findProduct($cond) {
        try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_product;
            if(isset($cond)) {
            	if(isset($cond['ids'])) {
            		$sql.= " where id in (". $cond['ids'] . ")";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['id'])) {
            		$sql.= " where id = ". $cond['id'];
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['flag_top_week'])) {
            		$sql.= " where flag_top_week = '". $cond['flag_top_week']."'";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['flag_newest'])) {
            		$sql.= " where flag_newest = '". $cond['flag_newest']."'";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['flag_saleoff'])) {
            		$sql.= " where percent_sale_off > 0";
            		$sql.= " and enable_flag = 't' order by percent_sale_off desc limit 0, 4";
            	} else if(isset($cond['flag_top_buy_week'])) {
            		$sql.= " where flag_top_buy_week = '". $cond['flag_top_buy_week']."'";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['flag_bentre'])) {
            		$sql.= " where flag_bentre = '". $cond['flag_bentre']."'";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['tag'])) {
            		$sql.= " where id in ( select product_id from tag where tag_key = '". $cond['tag']."' )";
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	}  else if(isset($cond['cate_id'])) {
            		$sql.= " where category_id = ". $cond['cate_id'];
            		$sql.= " and enable_flag = 't'";
            		$sql.= " order by order_in_list asc";
            	} else if(isset($cond['enable_flag'])) {
            		$sql.= " where enable_flag = '". $cond['enable_flag']."'";
            		$sql.= " order by order_in_list asc";
            	}
            } else {
                $sql.= " where enable_flag = 't'";
                $sql.= " order by order_in_list asc";
            }
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function putCart($data) {
     try {
            $db = $this->_getDbConnection();
            $rt=$db->insert(Product_Storages_MysqlImpl::$_cart, $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findCart($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_cart;
            if(isset($cond)) {
            	if(isset($cond['order_date'])) {
            		$sql.= " where order_date = '". $cond['order_date'] . "'";
            	}
            }
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function putCartDetail($data) {
     try {
            $db = $this->_getDbConnection();
            $rt=$db->insert(Product_Storages_MysqlImpl::$_cart_detail, $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    // admin
    public function updateProduct($data) {
        try {
            $db = $this->_getDbConnection();
            $sql = "update ".Product_Storages_MysqlImpl::$_product." set full_desc='" . $data["full_desc"] . "' where id = " . $data["id"];
            $result = $db->fetchAll($sql);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findKeyValue($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_key_value;
            if(isset($cond)) {
            	if(isset($cond['key'])) {
            		$sql.= " where key_value = '". $cond['key'] . "'";
            	}
            }
            $result = $db->fetchAll($sql);//var_dump($result);exit;
            return $result;
        } catch (Exception $ex) {//var_dump($ex);exit;
            throw $ex;
        }
    }

    /**
     * Find categroy by condition.
     * 
     * @param $cond Condition to filter
     */
    public function findCategory($cond) {
        try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_category;
            if(isset($cond)) {
            	if(isset($cond['ids'])) {
            		$sql.= " where id in (". $cond['ids'] . ")";
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['cate_id'])) {
            		$sql.= " where id = ". $cond['cate_id'];
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['flag_top_week'])) {
            		$sql.= " where flag_top_week = '". $cond['flag_top_week']."'";
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['flag_newest'])) {
            		$sql.= " where flag_newest = '". $cond['flag_newest']."'";
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['flag_saleoff'])) {
            		$sql.= " where percent_sale_off > 0";
            		$sql.= " and enable_flag = 't' order by percent_sale_off desc limit 0, 4";
            	} else if(isset($cond['flag_top_buy_week'])) {
            		$sql.= " where flag_top_buy_week = '". $cond['flag_top_buy_week']."'";
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['flag_bentre'])) {
            		$sql.= " where flag_bentre = '". $cond['flag_bentre']."'";
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['tag'])) {
            		$sql.= " where id in ( select product_id from tag where tag_key = '". $cond['tag']."' )";
            		$sql.= " and enable_flag = 't'";
            	}  else if(isset($cond['cate_id'])) {
            		$sql.= " where category_id = ". $cond['cate_id'];
            		$sql.= " and enable_flag = 't'";
            	} else if(isset($cond['enable_flag'])) {
            		$sql.= " where enable_flag = '". $cond['enable_flag']."'";
            		$sql.= " order by order_in_list asc";
            	}
            } else {
                $sql.= " where enable_flag = 't'";
                $sql.= " order by order_in_list asc";
            }
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function putEmailSubcriber($data) {
     try {
            $db = $this->_getDbConnection();
            return $db->insert(Product_Storages_MysqlImpl::$_email_subcriber, $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findEmailSubcriber($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_email_subcriber;
            if(isset($cond)) {
            	if(isset($cond['email'])) {
            		$sql.= " where email = '". $cond['email'] . "'";
            	}
            }
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findNews($cond) {
         try {
            $db = $this->_getDbConnection();
            $sql = "select * from ".Product_Storages_MysqlImpl::$_news;
            
            if(isset($cond)) {
                if(isset($cond['is_danh_sach'])) {
                     $sql = "select id, title, short_desc, image_description, create_date from ".Product_Storages_MysqlImpl::$_news;
                }
            	if(isset($cond['id'])) {
            		$sql.= " where id = ". $cond['id'];
            		$sql.= " and category_id = ". $cond['category_id'];
            		$sql.= " and enable_flag = '". $cond['enable_flag'] . "'";
            	}
            }
            $sql .= " order by create_date desc";
            //var_dump($sql);exit;
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }


    // OLD CODE
    public function getHotDeal() {
        try {
            $db = $this->_getDbConnection();
            $sql = "select * from account";
            $result = $db->fetchAll($sql);
            return $result[0];
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getDeal($page, $item_per_page) {
        try {
            if ($page > 1)
                $from = ($page - 1) * $item_per_page + 1;
            else
                $from = 0;
            $db = $this->_getDbConnection();
            $sql = "select * from deal where open > 0 and is_hot =0 order by order_idx asc"
                    . " limit $from,$item_per_page";
            $result = $db->fetchAll($sql); //var_dump($from,$item_per_page, count($result));exit;
            return $result;
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getDetailDeal($id) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from("deal");
            $select->where('open >0 and id=' . $id);
            $result = $db->fetchAll($select);
            return $result[0];
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getNewDeal($id, $maxItem) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from("deal", array('id', 'title', 'old_price', 'new_price', 'short_desc', 'image'));
            $select->where('id !=' . $id);
            $select->limitPage(1, $maxItem);
            $select->order('time_update desc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getSameDeal($id, $type, $maxItem) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from("deal", array('id', 'title', 'old_price', 'new_price', 'short_desc', 'image'));
            $select->where('id !=' . $id . ' and type=' . $type);
            $select->limitPage(1, $maxItem);
            $select->order('time_update desc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getCategories($enable, $on_menu) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_category, array('id', 'name'));
            $select->where('enable =' . $enable . ' and on_menu=' . $on_menu);
            $select->order('id asc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getNew($page, $item_per_page) {
        try {
            if ($this->isSpecial($page))
                $page = 1;
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_product, array('id', 'name', 'image', 'category', 'price', 'add_date'));
            $select->where('enable =1');
            $select->order('add_date desc');
            $select->limitPage($page, $item_per_page);
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getTotalNewProduct() {
        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total from " . self::$_product . " where enable > 0";
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getProductByCategory($catId, $page, $item_per_page) {

        try {
            if ($this->isSpecial($page))
                $page = 1;
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_product, array('id', 'name', 'image', 'category', 'price', 'add_date'));
            $select->where('enable =1 and category=' . $catId);
            $select->order('add_date desc');
            $select->limitPage($page, $item_per_page);
            $result = $db->fetchAll($select); //echo "<pre>"; var_dump($result);exit;
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getTotalProductByCategory($catId) {

        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total from " . self::$_product . " where category=$catId and enable>0";
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getSaleOffProduct($page, $item_per_page) {
        try {
            if ($this->isSpecial($page))
                $page = 1;
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_product, array('id', 'name', 'image', 'category', 'price', 'add_date', 'new_price'));
            $select->where('enable =1 and is_saleoff > 0');
            $select->order('from_time asc');
            $select->limitPage($page, $item_per_page);
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getTotalSaleOffProduct() {
        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total from " . self::$_product . " where is_saleoff > 0 and enable>0";
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getFocusProduct($page, $item_per_page) {
        try {
            if ($this->isSpecial($page))
                $page = 1;
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_product, array('id', 'name', 'image', 'price', 'add_date'));
            $select->where('enable =1');
            $select->order('click_count desc');
            $select->limitPage($page, $item_per_page);
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getTotalFocusProduct() {
        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total from " . self::$_product . " where enable > 0";
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getInfo($product_id) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_product);
            $select->where('enable =1 and id=' . $product_id);
            $result = $db->fetchAll($select);
            return $result[0];
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getCapChar($id) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from('capchar', array('value'));
            $select->where('id = ' . $id);
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function putContact($data) {
        try {
            $db = $this->_getDbConnection();
            $db->insert('contact', $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function putCard($data) {
        try {
            $db = $this->_getDbConnection();
            $db->insert('card', $data);
        } catch (Exception $ex) {
            var_dump($ex);
            exit;
            throw $ex;
        }
    }

    public function putMessage($data) {
        try {
            $db = $this->_getDbConnection();
            $db->insert('message', $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function searchProduct($keyword, $page, $item_per_page) {

        try {
            if ($page > 1)
                $from = ($page - 1) * $item_per_page + 1;
            else
                $from = 0;
            //$to=$page*$item_per_page;
            $db = $this->_getDbConnection();
            $sql = "select a.id as id, a.name as name, a.image as image, a.category as category," .
                    " a.price as price, a.add_date as add_date,b.name as namecat from "
                    . self::$_product . " a," . self::$_category . " b  where a.category=b.id and " .
                    " a.enable > 0 and a.name like '%" . $keyword . "%' " .
                    //  " group by a.id, a.name, a.image, a.category, a.price, a.add_date,b.name".
                    " limit $from,$item_per_page";
            // var_dump($sql);exit;
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $ex) {
            return 0;
        }

//        try {
//            $db = $this->_getDbConnection();
//            $select = $db->select();
//            $select->from(self::$_product, array('id', 'name', 'image', 'category', 'price', 'add_date'));
//            $select->where("enable =1 and name like '%" . $keyword."%'");
//            $select->order("add_date desc");
//            $select->limitPage($page, $item_per_page);
//            $result = $db->fetchAll($select); //var_dump($result);exit;
//            return $result;
//        } catch (Exception $e) {
//            throw $e;
//        }
    }

    public function searchTotal($keyword) {

        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total from " . self::$_product . " where enable > 0 and name like '%" . $keyword . "%'";
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getCity() {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from('city');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateCheckout($data) {
        try {
            $db = $this->_getDbConnection();
            $sql = "update card set verify=1, credit=" . $data["credit"] . " where id  like '" . $data["id"] . "'";
            $result = $db->fetchAll($sql);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getShopInfo() {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from('shopinfo');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getHistory($uname, $page) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_card, array('credit', 'time', 'time_give', 'status', 'amount'));
            $select->where("uname like '" . $uname . "'");
            $select->order('time desc');
            $select->limitPage($page, MAX_ITEM_HISTORY_PER_PAGE);
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getSlider() {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_slider);
            $select->where("enable >0");
            $select->order('order_idx asc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getByParent($parentId) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_category, array('id', 'name'));
            $select->where("parent_id = " . $parentId . " and enable >0 and on_menu>0");
            $select->order('order asc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getProByRootCat($catId, $page, $item_per_page) {

        try { //var_dump($item_per_page);exit;
            if ($this->isSpecial($page))
                $page = 1;
            if ($page > 1)
                $from = ($page - 1) * $item_per_page + 1;
            else
                $from = 0;

            $db = $this->_getDbConnection();
            $sql = "select id, name, image, category, price, new_price,	is_saleoff " .
                    " from product " .
                    "where category in (select id from category where parent_id =" . $catId . ")" .
                    " limit $from,$item_per_page";
            $result = $db->fetchAll($sql);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getTotalProByRootCat($catId) {

        try {
            $db = $this->_getDbConnection();
            $sql = "select count(*) as total " .
                    " from product " .
                    "where category in (select id from category where parent_id =" . $catId . ")";
            // var_dump("abc",$sql);exit;
            $result = $db->fetchAll($sql);
            return $result[0]['total'];
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getSubCat($id) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_category, array('id', 'name'));
            $select->where("parent_id = " . $id . " and enable >0 and on_menu>0");
            $select->order('order asc');
            $result = $db->fetchAll($select);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getSupCat($id) {
        try {
//            $db = $this->_getDbConnection();
//            $select = $db->select();
//            $select->from(self::$_category, array('parent_id'));
//            $select->where("id = " . $id);
//            $result = $db->fetchAll($select);

            $db = $this->_getDbConnection();
            $sql = "select id,name  " .
                    " from category " .
                    "where id in (select parent_id from category where id =" . $id . ")";
            // var_dump("abc",$sql);exit;
            $result = $db->fetchAll($sql); //var_dump($result);exit;
            return $result[0];

//            return $result[0]["parent_id"];
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function isSpecial($page) {
        if ($page == 0 || $page == ID_SAME_KIND)
            return true;
        return false;
    }

}
