<?php

interface Product_Storages_Interface {

    public function getCategories($enable, $on_menu);

    public function getNew($page, $item_per_page);

    public function getTotalNewProduct();

    public function getProductByCategory($catId, $page, $item_per_page);

    public function getTotalProductByCategory($catId);

    public function getSaleOffProduct($page, $item_per_page);

    public function getTotalSaleOffProduct();

    public function getFocusProduct($page, $item_per_page);

    public function getTotalFocusProduct();

    public function getInfo($product_id);

    public function getCapChar($id);

    public function putContact($data);
    
     public function putCard($data);

    public function searchProduct($keyword, $page, $item_per_page);

    public function searchTotal($keyword);

    public function getCity();
    
    public function updateCheckout($id);
    public  function putMessage($data);
}