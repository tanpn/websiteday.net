<?php

class Product_Storages_Factory {

    public static function factory($type = 'mysqli') {
        if ($type == 'mysqli') {
            return Product_Storages_MysqlImpl::getInstance();
        } else {
            return NULL;
        }
    }
}
?>