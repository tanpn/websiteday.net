<?php

class Product_Business {
    const CACHE_TYPE = "zendcache";
    const DB_TYPE = "mysqli";
    private $_cache;
    private $_db;
    static private $_instance = NULL;

    protected function __construct() {
        $this->_cache = Product_Caches_Factory::factory(self::CACHE_TYPE);
        $this->_db = Product_Storages_Factory::factory(self::DB_TYPE);
    }

    static public function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /** Themes **/
    public function findThemes($cond) {
    	$data = $this->_db->findThemes($cond);
    	return $data;
    }

    static function buildLinkDetailTheme($name,$id) {
        $name = ltrim($name);
        $name = rtrim($name);
        $name = Common::vn_str_filter($name);
        $name = strtolower($name);
        $name=str_replace("%", "phan-tram", $name);
        return BASE_URL . "/giao-dien/" . str_replace(" ", "-", $name) . "-" . $id . ".html";
    }

    static function buildLinkDemoTheme($name,$id) {
        $name = ltrim($name);
        $name = rtrim($name);
        $name = Common::vn_str_filter($name);
        $name = strtolower($name);
        $name=str_replace("%", "phan-tram", $name);
        return BASE_URL . "/demo/" . str_replace(" ", "-", $name) . "-" . $id . ".html";
    }

	public function findProduct($cond) {
	    $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

	public function findProductTopWeek($cond) {
	    $cond['flag_top_week']='t';
	    $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

    public function findProductTopBuyWeek($cond) {
        $cond['flag_top_buy_week']='t';
        $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

    public function findProductBeTre($cond) {
        $cond['flag_bentre']='t';
        $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

	public function findProductNewest($cond) {
	    $cond['flag_newest']='t';
	    $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }
    
	public function findProductSaleOff($cond) {
	    $cond['flag_saleoff']='t';
	    $cond['enable_flag']='t';
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

    public function findProductByCategory($cond) {
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

	public function putCart($data) {
		$data = $this->_db->putCart($data);
    	return $data;
	}

	public function findCartByDate($date) {
	    $cond['order_date'] = $date;
	    return $this->_db->findCart($cond);
	}
	
    public function putCartDetail($data) {
		$data = $this->_db->putCartDetail($data);
    	return $data;
	}
	
    public function updateProduct($data) {
		$data = $this->_db->updateProduct($data);
    	return $data;
	}

    public function findKeyValue($cond) {
    	$data = $this->_db->findKeyValue($cond);
    	return $data;
    }

    public function findProductByTag($cond) {
    	$data = $this->_db->findProduct($cond);
    	return $data;
    }

	public function putEmailSubcriber($data) {
		$data = $this->_db->putEmailSubcriber($data);
    	return $data;
	}
	
	public function findEmailSubcriber($data) {
		$data = $this->_db->findEmailSubcriber($data);
    	return $data;
	}

    //-------------- CATEGORY ------------------
    public function findCategory($cond) {
    	$data = $this->_db->findCategory($cond);
    	return $data;
    }
    //-------------- END CATEGORY -------------
    
    
    // NEWS
    public function findNews($cond) {
    	$data = $this->_db->findNews($cond);
    	return $data;
    }

    // Old code
    public function getHotDeal() {
        return $this->_db->getHotDeal();
    }
    
    public function getDeal($page, $item_per_page) {
        return $this->_db->getDeal($page, $item_per_page);
    }
    
     public function getDetailDeal($id) {
        return $this->_db->getDetailDeal($id);
    }
    
     public function getNewDeal($id,$maxItem){
        return $this->_db->getNewDeal($id,$maxItem);    
    }
    
     public function getSameDeal($id,$type,$maxItem){
        return $this->_db->getSameDeal($id,$type,$maxItem);    
    }
    
    
            
    //Lay nguyen 1 page
    public function getPage($key) {
        $key = KEY_CACHE_PAGE . $key;
        return $this->_cache->getCache($key);
    }

    //Set nguyen 1 page
    public function setPage($key, $value) {
        $key = KEY_CACHE_PAGE . $key;
        $data = $this->_cache->setCache($key, $value);
        return $data;
    }

    public function removePage($key) {
        $this->_cache->removeCache($key);
    }

    public function getCategories($enable=1, $on_menu=1) {
        $key = "cat_" . $enable . $on_menu;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getCategories($enable, $on_menu);
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getNew($page, $item_per_page) {
        $key = "new_" . $page;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getNew($page, $item_per_page);
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getTotalNewProduct() {
        $key = "totalnew";
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getTotalNewProduct();
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getProductByCategory($catId, $page, $item_per_page) {
        $key = "pbc_" . $catId . "_" . $page;
        $data = $this->_cache->getCache($key);
        if ($data == false) 
        {
            $data = $this->_db->getProductByCategory($catId, $page, $item_per_page); 
            $this->_cache->setCache($key, $data); 
        }
        return $data;
    }

    public function getTotalProductByCategory($catId) {
        $key = "total_pbc_" . $catId;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getTotalProductByCategory($catId);
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getSaleOffProduct($page, $item_per_page) {
        $key = "saleoff_" . $page;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getSaleOffProduct($page, $item_per_page);
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getTotalSaleOffProduct() {
        $key = "total_saleoff";
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getTotalSaleOffProduct();
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getFocusProduct($page, $item_per_page) {
        $key = "focus_" . $page;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getFocusProduct($page, $item_per_page);
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getTotalFocusProduct() {
        $key = "total_focus";
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getTotalFocusProduct();
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function getInfo($product_id) {
        $key = "prod_" . $product_id;
        $rs = $this->_cache->getCache($key);
        if ($rs == false) {
            $rs = $this->_db->getInfo($product_id);
            $this->_cache->setCache($key, $rs);
        }
        return $rs;
    }

    public function getCapchar($id) {
        $key = "capchar_$id";
        $rs = $this->_cache->getCache($key);
        if ($rs == false) {
            $rs = $this->_db->getCapchar($id);
            $rs = $rs[0]["value"];
            $this->_cache->setCache($key, $rs);
        }
        return $rs;
    }

    public function putContact($data) {
        $this->_db->putContact($data);
    }

    public function putCard($data) {
        $key = "invoid_" . $data["time"];
        $this->_cache->setCache($key, $data);
        $this->_db->putCard($data);
    }

    public function getCard($time) {
        $key = "invoid_" . $time;
        return $this->_cache->getCache($key);
    }

    public function searchProduct($keyword, $page, $item_per_page) {
        return $this->_db->searchProduct($keyword, $page, $item_per_page);
    }

    public function searchTotal($keyword) {
        return $this->_db->searchTotal($keyword);
    }

    public function click($id) {
        $key = "click_" . $id;
        $click = $this->_cache->getCache($key);
        if ($click == false) {
            $this->_cache->setCache($key, 1);
        } else {
            $click++;
            $this->_cache->setCache($key, $click);
        }
    }

    public function getCity() {
        $key = "city";
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getCity();
            $this->_cache->setCache($key, $data);
        }
        return $data;
    }

    public function updateCheckout($credit, $time) {
        $data = $this->getCard($time);
        $data["credit"] = $credit["credit"];
        $key = "invoid_" . $time;
        $this->_cache->setCache($key, $data);
        $this->_db->updateCheckout($credit);
    }

    public function putMessage($data) {
        $this->_db->putMessage($data);
    }

    public function getContact() {
        $key = "contactinfo";
        $rs = $this->_cache->getCache($key);
        if ($rs == false) {
            $rs = $this->_db->getShopInfo();
            $rs = $rs[0]["content"];
            $this->_cache->setCache($key, $rs);
        }
        return $rs;
    }

    public function getHistory($uname, $page) {
        return $this->_db->getHistory($uname, $page);
    }

    public function getSlider() {
        $key = "slider";
        $rs = $this->_cache->getCache($key);
        if ($rs == false) {
            $rs = $this->_db->getSlider();
            $this->_cache->setCache($key, $rs);
        }
        return $rs;
    }
    
    public function getMenu(){
        $key = "menu";
        $rs = $this->_cache->getCache($key);
        if ($rs == false) {
            $rs = $this->_db->getByParent(0);
            foreach($rs as &$item){
                $item["subitems"]=$this->_db->getByParent($item["id"]);
            }          
            $this->_cache->setCache($key, $rs);
        }// echo "<pre>"; var_dump($rs);exit;
        return $rs;
    }
    
    public function getProByRootCat($catId, $page, $item_per_page){
        $key = "pbr_" . $catId . "_" . $page;
        $data = $this->_cache->getCache($key); 
        //if ($data == false) 
        {
            $data = $this->_db->getProByRootCat($catId, $page, $item_per_page); 
            $this->_cache->setCache($key, $data); 
        }
        return $data;
    }
    
    public function getTotalProByRootCat($id){
        $key = "total_pbr_" . $id;
        $data = $this->_cache->getCache($key); 
        //if ($data == false) 
        {
            $data = $this->_db->getTotalProByRootCat($id); 
            $this->_cache->setCache($key, $data); 
        }
        return $data;
    }
    
    public function getSubCat($id){
        $key = "subcat_" . $id;
        $data = $this->_cache->getCache($key); 
        if ($data == false) 
        {
            $data = $this->_db->getSubCat($id); 
            $this->_cache->setCache($key, $data); 
        }
        return $data;
    }
    
     public function getSupCat($id){
        $key = "supcat_" . $id;
        $data = $this->_cache->getCache($key); 
        //if ($data == false) 
        {
            $data = $this->_db->getSupCat($id); 
            $this->_cache->setCache($key, $data); 
        }
        return $data;
    }
    
}

?>
