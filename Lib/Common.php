<?php

class Common {

    static function vn_str_filter($str) {
        $unicode = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd' => 'đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i' => 'í|ì|ỉ|ĩ|ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D' => 'Đ',
            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ'           
        );
        foreach ($unicode as $nonUnicode => $uni) {
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        return $str;
    }

    static function getDetailURL($str, $id) {
        $str = ltrim($str);
        $str = rtrim($str);
        $str = Common::vn_str_filter($str);
        $str = strtolower($str);
        $str=str_replace("%", "phan-tram", $str);
        return str_replace(" ", "-", $str) . "-" . $id . ".html";
    }

    static function getIdFromUrl($url) {
        if (strlen($url) > 0) {
            $arr = explode("-",$url);
            $n = count($arr);
            if (is_numeric($arr[$n - 1]))
                return intval($arr[$n - 1]);
            else
                return 0;
        }
        else
            return 0;
    }

    static function numberFormat($number = '') { 
        if (is_numeric($number)) {
            return number_format($number, 0, ',', '.');
        }
        return null;
    }

    static function getShortenedContent($content, $len) {
        if (strlen($content) <= $len)
            return $content;
        $content = iconv_substr($content, 0, $len, "UTF-8");
        $pos = strrpos($content, ' ', 0);
        if ($pos === false) {
            //Khong co khoang trang nao
            $content = iconv_substr($content, 0, $len - 3, "UTF-8") . "...";
        } else {
            //Co khoang trang
            $posTemp = strrpos($content, ' ', 0);
            if (strlen($content) > ($posTemp + 7)) {
                $content = iconv_substr($content, 0, $len - 3) . "...";
            } else {
                $content = substr($content, 0, $pos) . "...";
            }
        }
        return $content;
    }
    
    static function prodidFormat($prodid = '') {
        while (strlen($prodid) < 4) {
            $prodid = '0' . $prodid;
        }
        return "SP" . $prodid;
    }

    static function sendMail($email, $title, $content) {
        try {
            $tr = new Zend_Mail_Transport_Smtp(MAIL_BOX,
                            array(
                                'auth' => 'login',
                                'ssl' => 'ssl',
                                'port' => '465',
                                'username' => UNAME_SEND_MAIL,
                                'password' => PASS_SEND_MAIL));
            Zend_Mail::setDefaultTransport($tr);
            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom(ACC_SEND_MAIL);
            $mail->setBodyHtml($content);
            $mail->addTo($email);
            $mail->addCc(array(SHOP_EMAIL,SHOP_EMAIL_OWNER));
            $mail->setSubject($title);
            $mail->send();
        } catch (Exception $e) {
        }
    }

    static function sendInvoiceMail($invoice) {
        $title = "[DacSanDay.net] Thông tin đơn hàng DH" . $invoice["id"];
        $content = Common::getContentMail($invoice);// var_dump($content);exit;

        Common::sendMail($invoice["email"], $title, $content);
    }

    static function sendRecoverPassMail($uname, $pass, $email) {
        $title = "Thông tin khôi phục mật khẩu";
        $content = Common::getContentRecoverPassMail($uname, $pass, $email);
        Common::sendMail($email, $title, $content);
    }

    static function sendKhuyenMai($email) {
        $title = "Khuyến mãi lớn nhân dịp lễ quốc khánh 2/9";
        $content = Common::getContentKhuyenMai();
        Common::sendMail($email, $title, $content);
    }

    static function getContentMail($invoice) {

        $biz = Product_Business::getInstance();

        $time = $invoice["time"];

        $listProduct = $invoice['list_product'];
        $totalInvoice = $invoice['total_invoice'];

        $content =
                "<div>

            <div style='width:100%; background-color:#f4f4f4; border-bottom:2px solid #e9e9e9; overflow:hidden; height:50px'>

                <div class='x_top-mail'>

                    <p align='center' style='display:block; width:600px; height:50px'>Neu khong doc duoc tieng Viet, vui long chon menu View / Encoding. Chon charset la UTF-8.</p>

                </div>

            </div>

            <div style='margin-top:5px' id='x_page-mail'>

                <div style='padding-top:0px' class='x_logo-mail'><a target='_blank' style='float:left; margin-right:10px' href='" . BASE_URL . "'><img style='margin:15px 0px;width:140px' src='" . STATIC_DOMAIN . "/images/logo.png'></a>

                    <div style='margin-top:10px'>

                        <h3 style='font-size:16px; padding-top:14px'>Chào Bạn!</h3>

                        <p>Cám ơn bạn đã đặt mua hàng tại website <a target='_blank' title='" . SHOP_URL_SHORT . "' href='" . BASE_URL . " '>

                                " . SHOP_URL_SHORT . "</a> <span style='color:#e47911'>(Website chuyên bán đặc sản Bến Tre - Đặc sản 3 miền)</span></p>

                    </div>

                </div>

                <div style='clear:both'></div>

                <p>Thông tin đơn hàng <span style='font-weight:bold'>DH" . $invoice["id"] . "</span> đặt mua thành công vào

                    <span style='font-weight:bold'>" . $time . "</span> như sau:</p>

                <div class='x_ttdh'>

                    <h2>THÔNG TIN GIAO NHẬN</h2>

                    <table cellspacing='3' cellpadding='0' width='100%' style='border:1px solid #eee; border-top:2px solid #f99517; padding:5px 10px'>

                        <tbody>

                            <tr>

                                <td colspan='2'><strong>Thông tin người mua:</strong></td>

                                <td colspan='2'><strong>Thông tin người nhận:</strong></td>

                            </tr>

                            <tr>

                                <td width='60px;'><span style='text-decoration:underline'>Họ tên:</span></td>

                                <td align='left'>" . $invoice["name"] . "</td>

                                <td width='60px;'><span style='text-decoration:underline'>Họ tên:</span></td>

                                <td align='left'>" . $invoice["name"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Địa chỉ:</span></td>

                                <td align='left'>" . $invoice["address"] . "</td>

                                <td><span style='text-decoration:underline'>Địa chỉ:</span></td>

                                <td align='left'>" . $invoice["address"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Điện thoại:</span></td>

                                <td align='left'>" . $invoice["phone"] . "</td>

                                <td><span style='text-decoration:underline'>Điện thoại:</span></td>

                                <td align='left'>" . $invoice["phone"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Email:</span></td>

                                <td align='left'>" . $invoice["email"] . "</td>

                                <td><span style='text-decoration:underline'>Email:</span></td>

                                <td align='left'>" . $invoice["email"] . "</td>

                            </tr>

                        </tbody>

                    </table>

                </div>

                <div class='x_ttdh'>

                    <h2>THÔNG TIN SẢN PHẨM</h2>

                    <table cellspacing='0' cellpadding='10' width='100%' style='border:1px solid #eee; border-top:2px solid #f99517; padding:5px 10px; border-collapse:collapse'>

                        <tbody>

                            <tr style='text-align:center; background-color:#ecf6fe'>

                                <td align='left' width='15%' class='x_col1'>Mã sản phẩm</td>

                                <td align='left' width='40%' class='x_col2'>Tên sản phẩm</td>

                                <td align='left' width='15%' class='x_col3'>Đơn giá</td>

                                <td align='left' width='15%' class='x_col4'>Số lượng</td>

                                <td align='left' width='15%' class='x_col5'>Thành tiền</td>

                            </tr>";



        foreach ($listProduct as $itemProduct) {

            $content.=

                    "<tr>

                                    <td style='border-bottom:1px solid #eee; border-left:1px solid #eee' class='x_col1 x_padding'>" .
                    Common::prodidFormat($itemProduct["id"]) .
                    "</td>

                                    <td style='border-bottom:1px solid #eee' class='x_col2 x_padding'>" . $itemProduct["name"] . "</td>

                                    <td style='border-bottom:1px solid #eee' class='x_col3 x_padding'>

                                        <p>" . Common::numberFormat($itemProduct["new_price"]) . "</p>

                                    </td>

                                    <td style='border-bottom:1px solid #eee' class='x_col4 x_padding'>" . $itemProduct["quantity"] . "</td>

                                    <td style='border-bottom:1px solid #eee; border-right:1px solid #eee' class='x_col5 x_padding'>

                                        " . Common::numberFormat($itemProduct['sub_total']) . "<sup class='x_und'> đ </sup></td>

                                </tr>";
        }

        $content.=
"
<tr>
    <td colspan='4' style='text-align: right;'><p><strong style='font-weight:bold; color:#646464'>Tiền hàng:</strong></p></td>
   <td><p><strong>" . Common::numberFormat($totalInvoice) . "</strong>&nbsp;<sup class='x_und'>đ</sup></p></td>
</tr>

<tr>
    <td colspan='4' style='text-align: right;'><p><strong style='font-weight:bold; color:#646464'>Phí vận chuyển:</strong></p></td>
   <td><p><strong>" . $invoice['fee_ship'] . "</strong>&nbsp;<sup class='x_und'>đ</sup></p></td>
</tr>

<tr>
    <td colspan='4' style='text-align: right;'><p><strong style='font-weight:bold; color:#646464'>Tổng tiền sẽ thanh toán:</strong></p></td>
   <td><p><strong>" . Common::numberFormat($totalInvoice + $invoice['max_fee_ship']) . "</strong>&nbsp;<sup class='x_und'>đ</sup></p></td>
</tr>
                </tbody>

                    </table>
                </div>

                <div style='margin-bottom:20px; padding-top:10px; margin-top:10px' class='x_welcome'>
                    <p>* <i>Ghi chú cho đơn hàng:</i> ". $invoice["note"] . ".</p>
                    <p>Chúng tôi luôn hy vọng mang đến cho bạn những sản phẩm và dịch vụ với chất lượng tốt nhất.</p>

                    <p>Mọi thắc mắc xin vui lòng liên hệ hotline <strong>" . HOT_LINE . "</strong> để được hướng dẫn.</p>

                    <p>Chúng tôi hy vọng bạn đã có những giây phút trải nghiệm thú vị với dịch vụ mua sắm trực tuyến tại

                        <a target='_blank' href='" . BASE_URL . "'>

                            " . SHOP_URL_SHORT . "</a>.</p>

                    <br>

                    <br>

                    <strong>Trân trọng!</strong>

                    <p>Đội ngũ " . SHOP_URL_SHORT . "</p>

                </div>

                <br class='x_clear'>

            </div>

            <div id='x_footer-mail'><img height='53' width='37' style='float:left; display:block; margin-right:10px' src='" . STATIC_DOMAIN . "/images/logo.png'>

                <p>Địa chỉ: " . SHOP_STORE . "<br>" .
                "Hotline: " . HOT_LINE . "</p>

            </div>

        </div>

        <style>

            <!--

            .x_und

                {text-decoration:underline}

            .x_top-mail

                {width:670px;

                margin:0 auto;

                display:block;

                padding-top:25px;

                overflow:hidden;

                clear:both}

            #x_page-mail

                {width:670px;

                margin:0 auto;

                display:block}

            .x_ttdh

                {margin-top:15px}

            .x_inner-ttdh

                {border:1px solid #eee;

                border-top:2px solid #f99517;

                padding:5px 10px}

            .x_sums

                {width:300px;

                float:right;

                display:block}

            .x_inner-sums

                {color:#f50404;

                font-size:11px;

                float:right;

                display:block;

                width:250px;

                margin-bottom:5px;

                clear:both}

            .x_inner-sums

                {font-weight:normal;

                font-size:12px;

                color:#000}

            .x_inner-sums

                {color:#ac0404;

                font-weight:bold;

                font-size:12px}

            .x_col-left

                {text-align:right;

                display:block;

                margin-right:20px;

                width:520px;

                float:left}

            .x_col-right

                {text-align:right;

                float:left;

                display:block;

                width:100px}

            .x_infor-product

                {margin-top:20px}

            .x_col2, .x_col3, .x_col4

                {border-left:1px solid #fff;

                border-right:1px solid #eee}

            .x_col1

                {border-right:1px solid #eee}

            .x_col5

                {border-left:1px solid #fff}

            .x_col3, .x_col5

                {text-align:right;

                padding-right:15px}

            .x_col4

                {text-align:center}

            .x_padding

                {padding:15px}

            #x_footer-mail

                {width:670px;

                padding:20px 10px;

                margin:0px auto;

                display:block;

                height:55px;

                border-top:1px solid #dcdcdc;

                overflow:hidden;

                background-color:#f4f4f4;

                font-size:11px;

                color:#000}

            #x_footer-mail

                {line-height:18px}

            -->

        </style>

        ";

        return $content;
    }

    static function getContentMailInvoice($invoice) {

        $biz = Product_Business::getInstance();

        $time = date("d-m-Y H:i", $invoice["time"]);

        $infocard = $biz->getCard($invoice["time"]);

        $idcredit = $infocard["credit"];

        $waycredit = Util_Data::$waycredit[$idcredit];

        $total = 0;

        $pro = array();

        $selprod = $infocard["id"];

        if (isset($selprod)) {

            $selprod = base64_decode($selprod);

            $check = explode("-", $selprod);

            if (count($check) > 1 && strlen($check[0]) > 0 && strlen($check[1]) > 0) {

                $selprod = explode(",", $selprod);

                foreach ($selprod as $item) {

                    $tmp = explode("-", $item);

                    $info = $biz->getInfo(intval($tmp[0]));

                    $info["quantity"] = intval($tmp[1]);

                    $pro[] = $info;
                }
            }
        }

        $content =
                "<div>

            <div style='width:100%; background-color:#f4f4f4; border-bottom:2px solid #e9e9e9; overflow:hidden; height:50px'>

                <div class='x_top-mail'>

                    <p align='center' style='display:block; width:600px; height:50px'>Neu khong doc duoc tieng Viet, vui long chon menu View / Encoding. Chon charset la UTF-8.</p>

                </div>

            </div>

            <div style='margin-top:5px' id='x_page-mail'>

                <div style='padding-top:0px' class='x_logo-mail'><a target='_blank' style='float:left; margin-right:10px' href='" . BASE_URL . "'><img style='margin:15px 0px;width:140px' src='" . STATIC_DOMAIN . "/images/logo.jpg'></a>

                    <div style='margin-top:10px'>

                        <h3 style='font-size:16px; padding-top:14px'>Chào Bạn!</h3>

                        <p>Cám ơn bạn đã đặt mua hàng tại website <a target='_blank' title='" . SHOP_URL_SHORT . "' href='" . BASE_URL . " '>

                                " . SHOP_URL_SHORT . "</a> <span style='color:#e47911'>(Website chuyên bán đặc sản Bến Tre - Đặc sản 3 miền)</span></p>

                    </div>

                </div>

                <div style='clear:both'></div>

                <p>Thông tin đơn hàng <span style='font-weight:bold'>DH" . $invoice["id"] . "</span> đặt mua thành công vào

                    <span style='font-weight:bold'>" . $time . "</span> như sau:</p>

                <div class='x_ttdh'>

                    <h2>THÔNG TIN GIAO NHẬN</h2>

                    <table cellspacing='3' cellpadding='0' width='100%' style='border:1px solid #eee; border-top:2px solid #f99517; padding:5px 10px'>

                        <tbody>

                            <tr>

                                <td colspan='2'><strong>Thông tin người mua:</strong></td>

                                <td colspan='2'><strong>Thông tin người nhận:</strong></td>

                            </tr>

                            <tr>

                                <td width='60px;'><span style='text-decoration:underline'>Họ tên:</span></td>

                                <td align='left'>" . $invoice["name"] . "</td>

                                <td width='60px;'><span style='text-decoration:underline'>Họ tên:</span></td>

                                <td align='left'>" . $invoice["name"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Địa chỉ:</span></td>

                                <td align='left'>" . $invoice["address"] . "</td>

                                <td><span style='text-decoration:underline'>Địa chỉ:</span></td>

                                <td align='left'>" . $invoice["address"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Điện thoại:</span></td>

                                <td align='left'>" . $invoice["phone"] . "</td>

                                <td><span style='text-decoration:underline'>Điện thoại:</span></td>

                                <td align='left'>" . $invoice["phone"] . "</td>

                            </tr>

                            <tr>

                                <td><span style='text-decoration:underline'>Email:</span></td>

                                <td align='left'>" . $invoice["email"] . "</td>

                                <td><span style='text-decoration:underline'>Email:</span></td>

                                <td align='left'>" . $invoice["email"] . "</td>

                            </tr>

                        </tbody>

                    </table>

                </div>

                <div class='x_ttdh'>

                    <h2>THÔNG TIN SẢN PHẨM</h2>

                    <table cellspacing='0' cellpadding='10' width='100%' style='border:1px solid #eee; border-top:2px solid #f99517; padding:5px 10px; border-collapse:collapse'>

                        <tbody>

                            <tr style='text-align:center; background-color:#ecf6fe'>

                                <td align='left' width='15%' class='x_col1'>Mã sản phẩm</td>

                                <td align='left' width='40%' class='x_col2'>Tên sản phẩm</td>

                                <td align='left' width='15%' class='x_col3'>Đơn giá</td>

                                <td align='left' width='15%' class='x_col4'>Số lượng</td>

                                <td align='left' width='15%' class='x_col5'>Thành tiền</td>

                            </tr>";



        foreach ($pro as $item) {

            $sum = $item["price"] * $item["quantity"];

            $total+=$sum;

            $content.=

                    "<tr>

                                    <td style='border-bottom:1px solid #eee; border-left:1px solid #eee' class='x_col1 x_padding'>" .
                    Common::prodidFormat($item["id"]) .
                    "</td>

                                    <td style='border-bottom:1px solid #eee' class='x_col2 x_padding'>" . $item["name"] . "</td>

                                    <td style='border-bottom:1px solid #eee' class='x_col3 x_padding'>

                                        <p>" . Common::numberFormat($item["price"]) . "</p>

                                    </td>

                                    <td style='border-bottom:1px solid #eee' class='x_col4 x_padding'>" . $item["quantity"] . "</td>

                                    <td style='border-bottom:1px solid #eee; border-right:1px solid #eee' class='x_col5 x_padding'>

                                        " . Common::numberFormat($sum) . "<sup class='x_und'> đ </sup></td>

                                </tr>";
        }

        $content.=

                "</tbody>

                    </table>

                    <table border='0' width='100%' style='color:#f50404; font-size:12px; display:block; margin-bottom:5px; clear:both'>

                        <tbody>

                            <tr>

                                <td align='right'>

                                    <p style='color:#646464'>Phí giao hàng:</p>

                                </td>

                                <td align='right' style='margin-right:15px'>

                                    <p>0&nbsp;<sup class='x_und'>đ</sup> </p>

                                </td>

                            </tr>

                            <tr>

                                <td align='right' width='590'>

                                    <p><strong style='font-weight:bold; color:#646464'>Tiền hàng:</strong></p>

                                </td>

                                <td align='right' width='75' style='margin-right:15px'>

                                    <p><strong>" . Common::numberFormat($total) . "</strong>&nbsp;<sup class='x_und'>đ</sup></p>

                                </td>

                            </tr>
                            
                            <tr>

                                <td align='right' width='590'>

                                    <p><strong style='font-weight:bold; color:#646464'>Phí vận chuyển:</strong></p>

                                </td>

                                <td align='right' width='75' style='margin-right:15px'>

                                    <p><strong>" . $invoice['fee_ship'] . "</strong>&nbsp;<sup class='x_und'>đ</sup></p>

                                </td>

                            </tr>

                            <tr>

                                <td align='right' width='590'>

                                    <p><strong style='font-weight:bold; color:#646464'>Tổng tiền thanh toán:</strong></p>

                                </td>

                                <td align='right' width='75' style='margin-right:15px'>

                                    <p><strong>" . Common::numberFormat($total + $invoice['max_fee_ship']) . "</strong>&nbsp;<sup class='x_und'>đ</sup></p>

                                </td>

                            </tr>

                        </tbody>

                    </table>

                </div>

                <div style='margin-bottom:20px; padding-top:10px; margin-top:10px' class='x_welcome'>

                    <p style='display:none'><strong>Phương thức thanh toán:</strong> " . $waycredit . " </p>

                    <p style='display:none'><strong>Phương thức giao nhận hàng:</strong> Sau khi đơn hàng được ".SHOP_NAME." xác nhận lại với khách hàng, chúng tôi cam kết sẽ giao hàng miễn phí cho quý khách - áp dụng cho các quận huyện nội thành của HCM.</p>

                    <p>Chúng tôi luôn hy vọng mang đến cho bạn những sản phẩm và dịch vụ với chất lượng tốt nhất. Trường hợp bạn có yêu cầu về việc đổi trả sản phẩm, vui lòng tham khảo chính sách đổi trả hàng

                        <a target='_blank' href='" . BASE_URL . "/ho-tro/tra-hang/'>

                            tại đây</a>.</p>

                    <p>Mọi thắc mắc xin vui lòng truy cập vào địa chỉ <a target='_blank' href='" . BASE_URL . "/ho-tro/'>

                            " . BASE_URL . "/support/intro</a> hoặc liên hệ hotline " . HOT_LINE . " để được hướng dẫn.</p>

                    <p>Hãy là người đầu tiên nhận được thông tin về những sản phẩm mới nhất và chương trình khuyến mãi đặc biệt dành riêng cho thành viên của

                        <a target='_blank' href='" . BASE_URL . "'>

                            " . SHOP_URL_SHORT . "</a> bằng cách đăng kí nhận bản tin.</p>

                    <p>Chúng tôi hy vọng bạn đã có những giây phút trải nghiệm thú vị với dịch vụ mua sắm trực tuyến tại

                        <a target='_blank' href='" . BASE_URL . "'>

                            " . SHOP_URL_SHORT . "</a>.</p>

                    <br>

                    <br>

                    <strong>Trân trọng!</strong>

                    <p>Đội ngũ " . SHOP_URL_SHORT . "</p>

                </div>

                <br class='x_clear'>

            </div>

            <div id='x_footer-mail'><img height='53' width='37' style='float:left; display:block; margin-right:10px' src='" . STATIC_DOMAIN . "/images/logo.jpg'>

                <p>Địa chỉ: " . SHOP_STORE . "<br>" .
                "Hotline: " . HOT_LINE . "</p>

            </div>

        </div>

        <style>

            <!--

            .x_und

                {text-decoration:underline}

            .x_top-mail

                {width:670px;

                margin:0 auto;

                display:block;

                padding-top:25px;

                overflow:hidden;

                clear:both}

            #x_page-mail

                {width:670px;

                margin:0 auto;

                display:block}

            .x_ttdh

                {margin-top:15px}

            .x_inner-ttdh

                {border:1px solid #eee;

                border-top:2px solid #f99517;

                padding:5px 10px}

            .x_sums

                {width:300px;

                float:right;

                display:block}

            .x_inner-sums

                {color:#f50404;

                font-size:11px;

                float:right;

                display:block;

                width:250px;

                margin-bottom:5px;

                clear:both}

            .x_inner-sums

                {font-weight:normal;

                font-size:12px;

                color:#000}

            .x_inner-sums

                {color:#ac0404;

                font-weight:bold;

                font-size:12px}

            .x_col-left

                {text-align:right;

                display:block;

                margin-right:20px;

                width:520px;

                float:left}

            .x_col-right

                {text-align:right;

                float:left;

                display:block;

                width:100px}

            .x_infor-product

                {margin-top:20px}

            .x_col2, .x_col3, .x_col4

                {border-left:1px solid #fff;

                border-right:1px solid #eee}

            .x_col1

                {border-right:1px solid #eee}

            .x_col5

                {border-left:1px solid #fff}

            .x_col3, .x_col5

                {text-align:right;

                padding-right:15px}

            .x_col4

                {text-align:center}

            .x_padding

                {padding:15px}

            #x_footer-mail

                {width:670px;

                padding:20px 10px;

                margin:0px auto;

                display:block;

                height:55px;

                border-top:1px solid #dcdcdc;

                overflow:hidden;

                background-color:#f4f4f4;

                font-size:11px;

                color:#000}

            #x_footer-mail

                {line-height:18px}

            -->

        </style>

        ";

        return $content;
    }

    static function getHash($time) {
        return md5($time . "!123@abcshop");
    }

    static function getContentRecoverPassMail($uname, $pass, $email) {
        $time = time();
        $hash = Common::getHash($time);
        $content =
                "<div>
            <div style='width:100%; background-color:#f4f4f4; border-bottom:2px solid #e9e9e9; overflow:hidden; height:50px'>
                <div class='x_top-mail'>
                    <p align='center' style='display:block; width:600px; height:50px'>Neu khong doc duoc tieng Viet, vui long chon menu View / Encoding. Chon charset la UTF-8.</p>
                </div>
            </div>
            <div style='margin-top:5px' id='x_page-mail'>
                <div style='padding-top:0px' class='x_logo-mail'><a target='_blank' style='float:left; margin-right:10px' href='" . BASE_URL . "'><img style='margin:15px 0px;width:140px' src='" . STATIC_DOMAIN . "/images/logo.jpg'></a>
                    <div style='margin-top:10px'>
                        <h3 style='font-size:16px; padding-top:14px'>Chào Bạn!</h3>
                        <p>
                        Cám ơn bạn đã quan tâm và sử dụng dịch vụ của website mua sắm trực tuyến uy tín, đảm bảo
                        <a target='_blank' title='" . SHOP_URL_SHORT . "' href='" . BASE_URL . " '>" . SHOP_URL_SHORT . "</a>
                        .Sau đây là thông tin khôi phục lại mật khẩu bạn đã quên ứng với email bạn đã đăng ký.</span></p>
                    </div>
                </div>
                <div style='clear:both'></div>
                <br/>
                <div class='x_ttdh'>
                    <h2>THÔNG TIN MẬT KHẨU MỚI</h2>
                    <table cellspacing='3' cellpadding='0' width='100%' style='border:1px solid #eee; border-top:2px solid #f99517; padding:5px 10px'>
                        <tbody>
                            <tr>
                                <td colspan='2'><strong>Thông tin đăng nhập:</strong></td>
                            </tr>
                            <tr>
                                <td width='120px;'><span style='text-decoration:underline'>Tên đăng nhập:</span></td>
                                <td align='left'>" . $uname . "</td>
                            </tr>
                            <tr>
                                <td width='60px;'><span style='text-decoration:underline'>Mật khẩu mới:</span></td>
                                <td align='left'>" . $pass . "</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div style='margin-bottom:20px; padding-top:10px; margin-top:10px' class='x_welcome'>
                    <p>Bạn vui lòng nhấn vào đường link sau để tiến hành xác thực mật khẩu mới <a target='_blank' href='" . BASE_URL . "/auth/verifypass?uname=" . $uname . "&pass=" . $pass . "&email=" . $email . "&time=" . $time . "&hash=" . $hash . "'>
                            " . BASE_URL . "/verifypass?uname=" . $uname . "&pass=" . $pass . "&email=" . $email . "&time=" . $time . "&hash=" . $hash . "</a></p>
                    <br/>
                    <strong>Trân trọng!</strong>
                    <p>Đội ngũ " . SHOP_URL_SHORT . "</p>
                </div>
                <br class='x_clear'>
            </div>
            <div id='x_footer-mail'><img height='53' width='37' style='float:left; display:block; margin-right:10px' src='" . STATIC_DOMAIN . "/images/logo.jpg'>
                <p>Địa chỉ: " . SHOP_STORE . "<br>" .
                "Hotline: " . HOT_LINE . "</p>
            </div>
        </div>
        <style>
            <!--
            .x_und
                {text-decoration:underline}
            .x_top-mail
                {width:670px;
                margin:0 auto;
                display:block;
                padding-top:25px;
                overflow:hidden;
                clear:both}
            #x_page-mail
                {width:670px;
                margin:0 auto;
                display:block}
            .x_ttdh
                {margin-top:15px}
            .x_inner-ttdh
                {border:1px solid #eee;
                border-top:2px solid #f99517;
                padding:5px 10px}
            .x_sums
                {width:300px;
                float:right;
                display:block}
            .x_inner-sums
                {color:#f50404;
                font-size:11px;
                float:right;
                display:block;
                width:250px;
                margin-bottom:5px;
                clear:both}
            .x_inner-sums
                {font-weight:normal;
                font-size:12px;
                color:#000}
            .x_inner-sums
                {color:#ac0404;
                font-weight:bold;
                font-size:12px}
            .x_col-left
                {text-align:right;
                display:block;
                margin-right:20px;
                width:520px;
                float:left}
            .x_col-right
                {text-align:right;
                float:left;
                display:block;
                width:100px}
            .x_infor-product
                {margin-top:20px}
            .x_col2, .x_col3, .x_col4
                {border-left:1px solid #fff;
                border-right:1px solid #eee}
            .x_col1
                {border-right:1px solid #eee}
            .x_col5
                {border-left:1px solid #fff}
            .x_col3, .x_col5
                {text-align:right;
                padding-right:15px}
            .x_col4
                {text-align:center}
            .x_padding
                {padding:15px}
            #x_footer-mail
                {width:670px;
                padding:20px 10px;
                margin:0px auto;
                display:block;
                height:55px;
                border-top:1px solid #dcdcdc;
                overflow:hidden;
                background-color:#f4f4f4;
                font-size:11px;
                color:#000}
            #x_footer-mail
                {line-height:18px}
            -->
        </style>
        ";
        return $content;
    }

    static function getContentKhuyenMai() {
        $content =
                "<div>

            <div style='width:100%; background-color:#f4f4f4; border-bottom:2px solid #e9e9e9; overflow:hidden; height:50px'>

                <div class='x_top-mail'>

                    <p align='center' style='display:block; width:600px; height:50px'>Neu khong doc duoc tieng Viet, vui long chon menu View / Encoding. Chon charset la UTF-8.</p>

                </div>

            </div>

            <div style='margin-top:5px' id='x_page-mail'>

                <div style='padding-top:0px' class='x_logo-mail'><a target='_blank' style='float:left; margin-right:10px' href='" . BASE_URL . "'><img style='margin:15px 0px;width:140px' src='" . STATIC_DOMAIN . "/images/logo.jpg'></a>

                    <div style='margin-top:10px'>

                        <h3 style='font-size:16px; padding-top:14px'>Chào Bạn!</h3>

                        <p> Nhân dịp kỷ niệm lễ quốc khánh 2/9, <a target='_blank' title='" . SHOP_URL_SHORT . "' href='" . BASE_URL . " '>

                                " . SHOP_URL_SHORT . "</a> <span style='color:#e47911'>(dịch vụ mua bán quần áo và phụ kiện thời trang đảm bảo, uy tín)</span> có nhiều chương trình giảm giá đặc biệt dành cho quý khách hàng. </p>

                    </div>

                </div>

                <div style='clear:both'></div>

                <p>Để biết thêm thông tin, bạn vui lòng truy cập vào website: <a target='_blank' title='" . SHOP_URL_SHORT . "' href='" . BASE_URL . " '>

                                " . SHOP_URL_SHORT . "</a></p>                

                    <p>Hãy là người đầu tiên nhận được thông tin về những sản phẩm mới nhất và chương trình khuyến mãi đặc biệt dành riêng cho thành viên của

                        <a target='_blank' href='" . BASE_URL . "'>

                            " . SHOP_URL_SHORT . "</a> bằng cách đăng kí nhận bản tin.</p>

                    <p>Chúng tôi hy vọng bạn đã có những giây phút trải nghiệm thú vị với dịch vụ mua sắm trực tuyến tại

                        <a target='_blank' href='" . BASE_URL . "'>

                            " . SHOP_URL_SHORT . "</a>.</p>

                    <br>

                    <br>

                    <strong>Trân trọng!</strong>

                    <p>Đội ngũ " . SHOP_URL_SHORT . "</p>

                </div>

                <br class='x_clear'>

            </div>

            <div id='x_footer-mail'><img height='53' width='37' style='float:left; display:block; margin-right:10px' src='" . STATIC_DOMAIN . "/images/logo.jpg'>

                <p>Địa chỉ: " . SHOP_STORE . "<br>" .
                "Hotline: " . HOT_LINE . "</p>

            </div>

        </div>

        <style>

            <!--

            .x_und

                {text-decoration:underline}

            .x_top-mail

                {width:670px;

                margin:0 auto;

                display:block;

                padding-top:25px;

                overflow:hidden;

                clear:both}

            #x_page-mail

                {width:670px;

                margin:0 auto;

                display:block}

            .x_ttdh

                {margin-top:15px}

            .x_inner-ttdh

                {border:1px solid #eee;

                border-top:2px solid #f99517;

                padding:5px 10px}

            .x_sums

                {width:300px;

                float:right;

                display:block}

            .x_inner-sums

                {color:#f50404;

                font-size:11px;

                float:right;

                display:block;

                width:250px;

                margin-bottom:5px;

                clear:both}

            .x_inner-sums

                {font-weight:normal;

                font-size:12px;

                color:#000}

            .x_inner-sums

                {color:#ac0404;

                font-weight:bold;

                font-size:12px}

            .x_col-left

                {text-align:right;

                display:block;

                margin-right:20px;

                width:520px;

                float:left}

            .x_col-right

                {text-align:right;

                float:left;

                display:block;

                width:100px}

            .x_infor-product

                {margin-top:20px}

            .x_col2, .x_col3, .x_col4

                {border-left:1px solid #fff;

                border-right:1px solid #eee}

            .x_col1

                {border-right:1px solid #eee}

            .x_col5

                {border-left:1px solid #fff}

            .x_col3, .x_col5

                {text-align:right;

                padding-right:15px}

            .x_col4

                {text-align:center}

            .x_padding

                {padding:15px}

            #x_footer-mail

                {width:670px;

                padding:20px 10px;

                margin:0px auto;

                display:block;

                height:55px;

                border-top:1px solid #dcdcdc;

                overflow:hidden;

                background-color:#f4f4f4;

                font-size:11px;

                color:#000}

            #x_footer-mail

                {line-height:18px}

            -->

        </style>

        ";

        return $content;
    }

}

?>
