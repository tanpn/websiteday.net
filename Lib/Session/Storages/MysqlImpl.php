<?php

class Session_Storages_MysqlImpl implements Session_Storages_Interface {

    static protected $_instance = NULL;
    static protected $_user = "user";

    static public function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function _getDbConnection() {
        return Globals::getDbConnection(Zend_Registry::get('configuration')->db->instance, false);
    }

    public function getUser($txtacc,$txtpass) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_user);
            $select->where("uname like '" . $txtacc . "'");
            $select->where("pass like '" . $txtpass . "'");
            $result = $db->fetchAll($select); 
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function getUserByEmail($email) {
        try {
            $db = $this->_getDbConnection();
            $select = $db->select();
            $select->from(self::$_user);
            $select->where("email like '" . $email . "'");
            $result = $db->fetchAll($select); 
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function putUser($data) {
        try {
            $db = $this->_getDbConnection();
            $db->insert('user', $data);
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function updatePass($data) {
        try { 
            $db = $this->_getDbConnection();
            $db->update('user', $data,"uname like '" . $data["uname"]."'");
        } catch (Exception $ex) {
            throw  $ex;
        }
    }
    
    

}
