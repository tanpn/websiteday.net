<?php

class Session_Storages_Factory {

    public static function factory($type = 'mysqli') {
        if ($type == 'mysqli') {
            return Session_Storages_MysqlImpl::getInstance();
        } else {
            return NULL;
        }
    }

}

?>