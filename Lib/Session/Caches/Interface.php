<?php

interface Session_Caches_Interface {

    public function getCache($key);

    public function setCache($key, $value);

    public function removeCache($key);
   
}