<?php

class Session_Caches_Factory {

    public static function factory($type = 'zendcache') {
        if ($type == 'zendcache') {
            return Session_Caches_ZendcacheImpl::getInstance();
        }else {
            return NULL;
        }
    }

}

?>