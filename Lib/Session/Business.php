<?php

class Session_Business {
    const CACHE_TYPE = "zendcache";
    const DB_TYPE = "mysqli";
    private $_cache;
    private $_db;
    static private $_instance = NULL;

    protected function __construct() {
        $this->_cache = Session_Caches_Factory::factory(self::CACHE_TYPE);
        $this->_db = Session_Storages_Factory::factory(self::DB_TYPE);
    }

    static public function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getUser($txtacc, $txtpass) {
        $txtpass = md5($txtpass);
        $key = "user_" . md5($txtacc) . "_" . $txtpass;
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getUser($txtacc, $txtpass);
            if (isset($data) && count($data) > 0) {
                $this->_cache->setCache($key, $data);
            }
            else
                return false;
        }
        return $data;
    }

    public function getUserByEmail($email) {
        $key = "umail_" . md5($email);
        $data = $this->_cache->getCache($key);
        if ($data == false) {
            $data = $this->_db->getUserByEmail($email);
            if (isset($data) && count($data) > 0) {
                $this->_cache->setCache($key, $data);
            }
            else
                return false;
        }
        return $data;
    }

    public function updatePass($uname, $pass, $email) {
        $key = "umail_" . md5($email);
        $data = $this->_cache->getCache($key);

		if($data==false){
			 $data = $this->_db->getUserByEmail($email);	
		}
		
		//Delete cache user
		$key = "user_" . md5($data["uname"]) . "_" . $data["pass"];
        $this->_cache->removeCache($key);

		//Set cache email
		$key = "umail_" . md5($email);
        $data["pass"] = md5($pass);
        $this->_cache->setCache($key, $data);

		//Set cache user
		$key = "user_" . md5($data["uname"]) . "_" . $data["pass"];
        $this->_cache->setCache($key, $data);
        
        $this->_db->updatePass($data);
    }

    public function isLogin() {
        $auth = $_COOKIE["auth"]; 
        if (isset($auth)) {
            $auth = json_decode(base64_decode($auth));
            $auth = array(
                'uname' => $auth->uname,
                'pass' => $auth->pass
            );
            return $auth;
        } else {
            return false;
        }
    }

    public function logout() {
        try {
            setcookie("auth", "", time() - 3600, "/");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function putUser($data) {
        try {
            $key = "user_" . md5($data["uname"]) . "_" . $data["pass"];
            $this->_cache->setCache($key, $data);

            $key = "umail_" . md5($data["email"]);
            $this->_cache->setCache($key, $data);

            $this->_db->putUser($data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    //Modules admin
    public function isAdmin($uname) {
		$admin=Util_Data::$admin;
		foreach ($admin as $item) {
			if(strcasecmp($uname,$item)==0) {
				return true;
			}
		}
		return false;
	}

}

?>