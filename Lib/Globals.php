<?php

class Globals {

    private static $_dbName = null;
    private static $_db = null;
    public static $arrDB = array();

    static public function getConfiguration() {
        return Zend_Registry::get('configuration');
    }

    public static function setDbName($dbName) {
        self::$_dbName = $dbName;
        return self::$_dbName;
    }

    public static function getDbName() {
        return self::$_dbName;
    }

    public static function getDbConnection($dbName = '', $state = true, $iniConfigFile = '') {
        if(isset(self::$_db))
            return self::$_db;
        $config = Zend_Registry::get('configuration');
        self::$_db = Zend_Db::factory($config->maindb->adapter, array(
                    'host' => $config->maindb->params->host,
                    'username' => $config->maindb->params->username,
                    'password' => $config->maindb->params->password,
                    'dbname' => $config->maindb->params->dbname
                ));

        self::$_db->query('SET NAMES UTF8');
        return self::$_db;
    }


}

?>