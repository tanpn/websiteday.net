<?php

$frontController = Zend_Controller_Front::getInstance();
$frontController->setParam('env', APP_ENV);

// com module
$frontController->addControllerDirectory(APPLICATION_PATH . '/modules/default/controllers', 'default');
$frontController->addControllerDirectory(APPLICATION_PATH . '/modules/admin/controllers', 'admin');

$default_route = new Zend_Controller_Router_Route(':controller/:action',
                array('controller' => 'index', 'action' => 'index', 'module' => 'default'));
$cart_route = new Zend_Controller_Router_Route('gio-hang/:action',
                array('controller' => 'cart', 'action' => 'cart', 'module' => 'default'));
$checkout_route = new Zend_Controller_Router_Route('gio-hang/thanh-toan',
                array('controller' => 'cart', 'action' => 'thanhtoan', 'module' => 'default'));
$product_route = new Zend_Controller_Router_Route('san-pham.html',
                array('controller' => 'product', 'action' => 'listall', 'module' => 'default'));
$product_bentre_route = new Zend_Controller_Router_Route('dac-san-ben-tre.html',
                array('controller' => 'product', 'action' => 'spbentre', 'module' => 'default'));
$product_tag_route = new Zend_Controller_Router_Route('tag/:tagkey/',
                array('controller' => 'product', 'action' => 'tag', 'module' => 'default'));
$category_route = new Zend_Controller_Router_Route('danh-muc/:cateid',
                array('controller' => 'category', 'action' => 'index', 'module' => 'default'));

$support_quydinh_route = new Zend_Controller_Router_Route('quy-dinh-chung.html',
                array('controller' => 'support', 'action' => 'quydinhchung', 'module' => 'default'));
$support_lienhe_route = new Zend_Controller_Router_Route('lien-he/',
                array('controller' => 'support', 'action' => 'lienhe', 'module' => 'default'));
$support_vechungtoi_route = new Zend_Controller_Router_Route('ve-chung-toi.html',
                array('controller' => 'support', 'action' => 'vechungtoi', 'module' => 'default'));
                
$support_hoptackinhdoanh_route = new Zend_Controller_Router_Route('thu-moi-hop-tac-kinh-doanh.html',
                array('controller' => 'support', 'action' => 'hoptackinhdoanh', 'module' => 'default')); 
                
$support_mail_subcriber = new Zend_Controller_Router_Route('email-subcriber.html',
                array('controller' => 'support', 'action' => ' emailsubcriber', 'module' => 'default'));

$product_detail_route = new Zend_Controller_Router_Route('chi-tiet/san-pham/:id/',
                array('controller' => 'product', 'action' => 'chitiet', 'module' => 'default'),array ('id' => '\d+'));

$popup_route = new Zend_Controller_Router_Route('popup/:action',
                array('controller' => 'popup', 'action' => 'index', 'module' => 'default'));
$support_route = new Zend_Controller_Router_Route('support/:action',
                array('controller' => 'support', 'action' => 'index', 'module' => 'default'));
// admin
$admin_route = new Zend_Controller_Router_Route('admin/:controller/:action',
                array('controller' => 'index', 'action' => 'index', 'module' => 'admin'));
$admin_product_route = new Zend_Controller_Router_Route('admin/product/:action',
                array('controller' => 'product', 'action' => 'index', 'module' => 'admin'));
$admin_banner_route = new Zend_Controller_Router_Route('admin/banner/:action',
                array('controller' => 'banner', 'action' => 'index', 'module' => 'admin'));
$admin_information_route = new Zend_Controller_Router_Route('admin/information/:action',
                array('controller' => 'information', 'action' => 'index', 'module' => 'admin'));

// blog hoat dong
$hoatdong_route = new Zend_Controller_Router_Route_Regex(
    'hoat-dong',
    array(
        'controller' => 'tintuc',
        'action'     => 'danhsachhoatdong'
    )
);

// blog hoat dong
$hoatdong_detail_route = new Zend_Controller_Router_Route_Regex(
    'hoat-dong/(.+)\-(\d+).html',
    array(
        'controller' => 'tintuc',
        'action'     => 'chitiethoatdong'
    ),
    array(
        1 => 'description',
        2 => 'id'
    ),
    'hoat-dong/%s-%d.html'
);

//======= DICH VU ========
$dichvu_route = new Zend_Controller_Router_Route_Regex(
    'dich-vu',
    array(
        'controller' => 'tintuc',
        'action'     => 'danhsachdichvu'
    )
);

$dichvu_detail_route = new Zend_Controller_Router_Route_Regex(
    'dich-vu/(.+)\-(\d+).html',
    array(
        'controller' => 'tintuc',
        'action'     => 'chitietdichvu'
    ),
    array(
        1 => 'description',
        2 => 'id'
    ),
    'dich-vu/%s-%d.html'
);


// news
$meovat_detail_route = new Zend_Controller_Router_Route_Regex(
    'meo-vat/(.+)\-(\d+).html',
    array(
        'controller' => 'tintuc',
        'action'     => 'chitietmeovat'
    ),
    array(
        2 => 'id',
        1 => 'description'
    ),
    'meo-vat/%s-%d.html'
);

$category_route2 = new Zend_Controller_Router_Route_Regex(
    '(.+)\-(\d+)',
    array(
        'controller' => 'category',
        'action'     => 'index'
    ),
    array(
        1 => 'cate-name',
        2 => 'cateid'
    ),
    '/%s-%d/'
);

//======= GIAO-DIEN /giao-dien/ ========
$giaodien_route = new Zend_Controller_Router_Route_Regex(
    'giao-dien',
    array(
        'controller' => 'giaodien',
        'action'     => 'index'
    )
);

$giaodien_detail_route = new Zend_Controller_Router_Route_Regex(
    'giao-dien/(.+)\-(\d+).html',
    array(
        'controller' => 'giaodien',
        'action'     => 'chitietgiaodien'
    ),
    array(
        2 => 'id',
        1 => 'description'
    ),
    'giao-dien/%s-%d.html'
);

//======= DEMO /demo / ========
$demo_giaodien_route = new Zend_Controller_Router_Route_Regex(
    'demo/(.+)\-(\d+).html',
    array(
        'controller' => 'demogiaodien',
        'action'     => 'demogiaodien'
    ),
    array(
        2 => 'id',
        1 => 'description'
    ),
    'demo/%s-%d.html'
);

// add route
$router = $frontController->getRouter();
$router->addRoute('default', $default_route);
$router->addRoute('cart', $cart_route);
$router->addRoute('checkout', $checkout_route);
$router->addRoute('product', $product_route);
$router->addRoute('productbetre', $product_bentre_route);
$router->addRoute('productdetail', $product_detail_route);
$router->addRoute('producttag', $product_tag_route);
$router->addRoute('category', $category_route);
$router->addRoute('category2', $category_route2);
$router->addRoute('hoatdong_route', $hoatdong_route);
$router->addRoute('hoatdong_detail_route', $hoatdong_detail_route);
$router->addRoute('dichvu_route', $dichvu_route);
$router->addRoute('dichvu_detail_route', $dichvu_detail_route);
$router->addRoute('giaodien_route', $giaodien_route);
$router->addRoute('giaodien_detail_route', $giaodien_detail_route);
$router->addRoute('demo_giaodien_route', $demo_giaodien_route);

$router->addRoute('quydinhchung', $support_quydinh_route);
$router->addRoute('bando', $support_lienhe_route);
$router->addRoute('mail_subcriber', $support_mail_subcriber);
$router->addRoute('vechungtoi', $support_vechungtoi_route);
$router->addRoute('hoptackinhdoanh', $support_hoptackinhdoanh_route);

// admin
$router->addRoute('admin', $admin_route);
$router->addRoute('admin_product_route', $admin_product_route);
$router->addRoute('admin_banner_route', $admin_banner_route);
$router->addRoute('admin_information_route', $admin_information_route);

$router->addRoute('popup', $popup_route);
$router->addRoute('support', $support_route);

//$router->addRoute('admin_product_upload', $admin_product_upload);

// news
$router->addRoute('meovat_detail_route', $meovat_detail_route);

$frontController->setRouter($router);

// include define config file
require_once APPLICATION_PATH . '/etc/define.php';

$frontController->throwExceptions(true);

Zend_Layout::startMvc(
        array(
            'layoutPath' => APPLICATION_PATH . 'layouts/',
            'layout' => "layout"
        )
);

unset($frontController, $configuration, $avatarUtil, $router);