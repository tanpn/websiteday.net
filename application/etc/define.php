<?php

$defineConfig = Zend_Registry::get('configuration');

define('ACTIVE_MENU_CLASS', 'active');

// define business
define('SHOPPING_CART_COOKIE', 'cart');
define("MAX_ITEM_PER_PAGE",9);
define("SITE_NAME","Trustdeal.vn");
define("HOT_LINE","0939014689 (Ms Trâm)");

define('MAX_NEW_ON_DETAIL_PAGE', 6);
define('MAX_SAME_DEAL_ON_DETAIL_PAGE', 4);

//cache product
define('CACHE_EXPIRE', 99999999999);// cache forever
define('CACHE_SERIALIZE', true);
define('CACHE_DIR', ROOT_PATH.'/cache/product');

//cache session
define('CACHE_SESSION_EXPIRE', 99999999999);// cache forever
define('CACHE_SESSION_SERIALIZE', true);
define('CACHE_SESSION_DIR', ROOT_PATH.'/cache/session');
define("KEY_CACHE_PAGE","kcp_");

//static
define("STATIC_DOMAIN",BASE_URL);
define("IMAGE_URL",STATIC_DOMAIN . '/images');
define("JS_URL",STATIC_DOMAIN . '/js');

//info shop
define("KEY_WORD","Chuyên bán các loại quần áo rẻ, đẹp, hợp thời trang phù hợp với mọi lứa tuổi");
define("DESCRIPTION","Trang web chuyên bán đồ rẻ, đẹp, thời trang, mẫu mã đa dạng");
define("AUTHOR","quanaoxinh.net");
define("COPYRIGHT","Copyright (c) 20012 by ".AUTHOR);
define("COMPANY_NAME","Công ty MSoft");
define("COMPANY_WEBSITE","http://msoft.gamehay99.com");
define("SHOP_NAME","DacSanDay.net");
define("SHOP_URL","http://quanaoxinh.net");
define("SHOP_URL_SHORT","dacsanday.net");
define("HOT_LINE_ONLY_PHONE","0939014689");
define("SHOP_EMAIL","dacsanday.net@gmail.com");
define("SHOP_EMAIL_OWNER","truongthutram1987@gmail.com");
define("SHOP_STORE","861/48/12 Trần Xuân Soạn, P.Tân Hưng, Q.7, HCM (Gần siêu thị Lotte Mark Q.7)");
define("YAHOO1","truongthithu.tram");
define("YAHOO2","lovelytram2006");
define("ON_ERROR_YAHOO","notonline.gif");

//bussiness
define("MAX_ITEM_NEW",12);
define("MAX_ITEM_GROUP1",4);
define("MAX_ITEM_GROUP2",4);
define("MAX_ITEM_SALEOFF",3);
define("MAX_ITEM",99999999999);
define("MAX_ITEM_FOCUS",3);

define("MAX_ITEM_SAME_KIND",4);
define("ID_SAME_KIND",9999);
define("ID_GROUP_MAIN1",6);
define("ID_GROUP_MAIN2",7);
define("ID_PHU_KIEN",4);
define("NAME_GROUP_MAIN1","Áo dây");
define("NAME_GROUP_MAIN2","Váy đầm");
define("MAX_LEN_SEARCH",30);
define("SEL_PRODUCT","selprod");
define("ENABLE_LIKE_GOOGLE",false);
define("ENABLE_LIKE_FACEBOOK",false);
define("MAX_ITEM_HISTORY_PER_PAGE",20);

//Send email
define("MAIL_BOX","smtp.gmail.com");
define("UNAME_SEND_MAIL","dacsanday.net@gmail.com");
define("PASS_SEND_MAIL","04m085zezo@quynh");
define("ACC_SEND_MAIL","no-reply@dacsanday.net");

// Define category news
define("NEWS_CATEGORY_ID","1");
define("CATEGORY_ID_ACTIVITY","2");
define("CATEGORY_ID_SERVICE","3");

define("TIPS_CATEGORY_ID","9999");


//Manage verion css, js
//css version
define("CSS_MAIN_STYLE_VERSION","1.00");
define("CSS_PRODUCT_VERSION","1.01");
define("CSS_TEMPLATES_VERSION","1.03");

//js version
define("JS_VERSION","1.01");

?>
