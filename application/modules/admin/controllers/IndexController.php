<?php

class Admin_IndexController extends Zend_Controller_Action {

    private $auth;

    public function init() {
        $this->auth = Session_Business::getInstance()->isLogin();
        if ($this->auth != false) {
            $isAdmin = Session_Business::getInstance()->isAdmin($this->auth['uname']);
            if ($isAdmin) {
                $this->view->auth = $this->auth;
                Zend_Layout::startMvc(APPLICATION_PATH . 'layouts');
                $layout = Zend_Layout::getMvcInstance();
                $layout->setLayoutPath(APPLICATION_PATH . 'layouts')->setLayout('admin');
                $this->addCSS(STATIC_DOMAIN . '/css/admin/reset_1.00.css');
                $this->addCSS(STATIC_DOMAIN . '/css/admin/layout_1.00.css');
                $this->addCSS(STATIC_DOMAIN . '/css/admin/cate_1.00.css');
            } else {
                header("location:" . BASE_URL);
            }
        } else {
            header("location:" . BASE_URL);
        }
    }

    public function indexAction() {
        
    }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
