<?php

class Admin_CacheController extends Zend_Controller_Action {
	private $auth;
	private $isLogIn;
	private $defBiz;
	public function init() {
		$this->auth = Zing_Auth_Authv2::getInstance();
		$this->_helper->layout()->setLayout('admin/admin');
		$this->isLogIn=false;
		$isAdmin=false;
		if ($this->auth->isLogged()) {
			$this->isLogIn = true;
			$userInfo = $this->auth->getIdentity();
			$isAdmin=Bookmark_Utils_Utils::getInstance()->checkIsAdmin($userInfo['username']);
			if($isAdmin) {
				$this->userId = Bookmark_BmUsers_Business::getInstance()->getIdByZingAcc($userInfo);
				$this->view->userId = $this->userId;
				$this->view->userInfo = $userInfo;
			}else {
				header("location:".BASE_URL);
			}
		}else {
			header("location:".BASE_URL);
		}
		$this->view->isLogIn=$this->isLogIn;

		$this->addCSS(STATIC_BOOKMARK_URL . '/css/admin/reset_1.00.css');
		$this->addCSS(STATIC_BOOKMARK_URL . '/css/admin/layout_1.00.css');
		$this->addCSS(STATIC_BOOKMARK_URL . '/css/admin/cate_1.00.css');

		$this->addJS(STATIC_URL . '/v3/js/beta_config_production-' . VERSION_JS_CONFIG_PRODUCTION . '.js');
		$this->addJS(STATIC_URL . '/zmjs/zmCore-' . VERSION_JS_ZM_CORE_MIN . '.min.js');
		$this->addJS(STATIC_URL . '/zmjs/zm.ui-' . VERSION_JS_ZM_UI_MIN . '.min.js');
		if ($this->isLogIn == true)
			$this->addJS(WIDGET_URL . '/v3/js/zmwg-' . WIDGET_JS_VERSION . '.min.js');

	}

	public function indexAction() {
		$this->view->msg="&nbsp";
		if($this->getRequest()->isPost()) {
			$cache= $this->_request->getParam('cachename');
			switch ($cache) {
				case 'data':
					$this->clearData();
					break;
			}
			if($cache!='')
				$this->view->msg="Đã xóa xong cache";
		}
	}

	private function clearData() {
		//Clear item info
		$adminItemBiz=Bookmark_BmaItems_Business::getInstance();
		$rs = $adminItemBiz->getMaxId();
		$maxId=$rs[0]['uid'];
		for($i=1;$i<=$maxId;$i++) {
			$adminItemBiz->clearCacheInfo($i);
		}
		//Clear cache page IndexController
		$defCache=Bookmark_BmDef_Caches_MemcacheImpl::getInstance();
		$defCache->removeCache('cp_index_index');

		//Clear cache def data
		$defCache->removeCache('defdata');
		$defCache->removeCache('cp_keyhotlink');
		$defCache->removeCache('cp_news_index');
		$defCache->removeCache(KEY_TOP_LIST_DATA);
		$defCache->removeCache(KEY_BOTTOM_CATE);

		//Clear cache page CateController
		$rs=Bookmark_BmaCategories_Storages_MysqlImpl::getInstance()->getAllByParentId(0);
		foreach ($rs as $item)
			$defCache->removeCache('cp_cate_index'.$item['uid']);		
		
	}

	private function clearTopList() {
		$this->defBiz->removeCache(KEY_TOP_LIST_DATA);
		$this->defBiz->getEnableTopList();
	}

	private function clearBxhZingMp3() {
		$this->defBiz->removeCache(KEY_BXH_ZING_MP3);
		$this->defBiz->getBxhZingMp3();
	}

	protected function addCSS($css) {
		$this->cssLinks[] = $css;
	}

	protected function addJS($js) {
		$this->jsLinks[] = $js;
	}

	public function dispatch($action) {
		try {
			parent::dispatch($action);
		} catch (Exception $ex) {
			if ($ex->getCode() != 404) {

			}
			return;
		}
		$this->view->currentAction = $action;
		$this->view->cssLinks = $this->cssLinks;
		$this->view->jsLinks = $this->jsLinks;
	}
}
?>