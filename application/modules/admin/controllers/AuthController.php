<?php

class Admin_AuthController extends Zend_Controller_Action {
	public function init() {
	}

	public function indexAction() {
		if($auth!=1) {
			$this->_helper->layout()->setLayout('admin/admin_login');
		}
		else {
		}
	}

	public function loginAction() {
		$request 	= $this->getRequest();
		$registry 	= Zend_Registry::getInstance();
		$auth		= Zend_Auth::getInstance();

		$db = Globals::getDbConnection('maindb');
		$authAdapter = new Zend_Auth_Adapter_DbTable($db);
		$authAdapter->setTableName('zfw_users')
				->setIdentityColumn('username')
				->setCredentialColumn('password');

		// Set the input credential values
		$uname = $request->getParam('username');
		$paswd = $request->getParam('password');
		$authAdapter->setIdentity($uname);
		$authAdapter->setCredential(md5($paswd));

		// Perform the authentication query, saving the result
		$result = $auth->authenticate($authAdapter);


		if($result->isValid()) {
			$data = $authAdapter->getResultRowObject(null, 'password');
			$auth->getStorage()->write($data);
			$this->_redirect('/admin/home');
		}
		else {
			$this->_redirect('/admin/auth');
		}
	}

	public function logoutAction() {
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->_redirect('/admin/auth');
	}
}
?>