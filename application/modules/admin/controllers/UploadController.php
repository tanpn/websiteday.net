<?php

class Admin_UploadController extends Zend_Controller_Action {

    private $logger;

    public function init() {
        $this->logger = Zend_Registry::get('logger');

        Zend_Layout::startMvc(APPLICATION_PATH . 'layouts');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayoutPath(APPLICATION_PATH . 'layouts')->setLayout('admin');

        $this->addJS(STATIC_DOMAIN . '/js/admin/jquery/jquery-2.1.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/admin/bootstrap/js/bootstrap.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/admin/jquery/datetimepicker/moment/moment.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/admin/jquery/datetimepicker/moment/moment-with-locales.min.js');
         $this->addJS(STATIC_DOMAIN . '/js/admin/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
         $this->addJS(STATIC_DOMAIN . '/js/admin/common.js');
         $this->addJS(STATIC_DOMAIN . '/js/admin/codemirror/lib/codemirror.js');
          $this->addJS(STATIC_DOMAIN . '/js/admin/codemirror/lib/xml.js');
           $this->addJS(STATIC_DOMAIN . '/js/admin/codemirror/lib/formatting.js');
             $this->addJS(STATIC_DOMAIN . '/js/admin/summernote/summernote.js');
              $this->addJS(STATIC_DOMAIN . '/js/admin/summernote/summernote-image-attributes.js');
               $this->addJS(STATIC_DOMAIN . '/js/admin/summernote/opencart.js');
               $this->addJS(STATIC_DOMAIN . '/js/admin/product-1.00.js');

        $this->addCSS(STATIC_DOMAIN . '/css/admin/stylesheet.css');
        $this->addCSS(STATIC_DOMAIN . '/css/admin/bootstrap.css');
         $this->addCSS(STATIC_DOMAIN . '/js/admin/font-awesome/css/font-awesome.min.css');
           $this->addCSS(STATIC_DOMAIN . '/js/admin/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
            $this->addCSS(STATIC_DOMAIN . '/js/admin/codemirror/lib/codemirror.css');
             $this->addCSS(STATIC_DOMAIN . '/js/admin/codemirror/theme/monokai.css');
     $this->addCSS(STATIC_DOMAIN . '/js/admin/summernote/summernote.css');
  
        
    }

    /**
	 * Test upload file.
	 */
    public function uploadAction() {
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       header("Content-type:text/html");

       $image_path_upload = $_SERVER['DOCUMENT_ROOT']."/images/upload/product/". date('Ymd');
       $temp_path_upload = $_SERVER['DOCUMENT_ROOT']."/images/upload/product". "/tmp/" . date('Ymd'); 
       if (!file_exists ($image_path_upload) || !is_dir( $image_path_upload) ) {
            mkdir( $image_path_upload, 0755, true );       
       }
       if (!file_exists ($temp_path_upload) || !is_dir( $temp_path_upload) ) {
            mkdir( $temp_path_upload, 0755, true );       
       }

        $temp_file_name = time() . basename($_FILES["fileToUpload"]["name"]);
        $target_file = $temp_path_upload . "/". $temp_file_name;
        
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));//var_dump($imageFileType,$image_path_upload,$temp_path_upload);exit;
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        } 
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                
                $image = new ResizeImage();
	
            	$image->init($target_file);
            	
                $arr_size[0]['w'] = 250;
                $arr_size[0]['h'] = 250;
                $arr_size[1]['w'] = 400;
                $arr_size[1]['h'] = 250;
                $arr_size[2]['w'] = 600;
                $arr_size[2]['h'] = 800;
                $arr_size[3]['w'] = 1016;
                $arr_size[3]['h'] = 940;
                 $arr_size[4]['w'] = 100;
                $arr_size[4]['h'] = 100;
                
                //$file_name = "hinh-test";
                $file_name =  md5(time());
                $date = date('Ymd');
                foreach($arr_size as $size) {
                    $image->init($target_file);
                    $file_dest = $image_path_upload . "/" . $file_name ."-". $size['w'] . "x" . $size['h']. ".jpg";
                    $image->resize_max_width_max_height($size['w'],$size['h']);
            	    $image->save($file_dest);
                }

                $array_response = array(
                    "date" => $date,
                    "file_name" => $file_name,
                    "image_file_type" => $imageFileType
                );
                // Convert Array to JSON String
                $string_json_response = json_encode($array_response);
            	
            	echo "<script>parent.adminProduct.setImageDataUpload" . "('" . json_encode($array_response) ."');</script>";
                
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}


class ResizeImage {
   
	private $image;
	private $image_type;
 
	public function init($filename) {
		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		
		switch($this->image_type) {
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($filename);
				break;
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($filename);
				break;
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($filename);
				break;
		}
	}
	
	public function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
		switch($image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image, $filename, $compression);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image,$filename);
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image,$filename);
				break;
			default:
				return false;
				break;
		}
		
		if($permissions != null) {
			chmod($filename, $permissions);
		}
		
		return true;
	}
	public function output($image_type = IMAGETYPE_JPEG) {
		switch($image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image); 
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image);
				break;
		}  
	}
	public function get_width() {
		return imagesx($this->image);
	}
   
	public function get_height() {
		return imagesy($this->image);
	}
	
	public function resize_to_height($height) {
		$ratio = $height / $this->get_height();
		$width = $this->get_width() * $ratio;
		$this->resize($width, $height);
	}
   
	public function resize_to_width($width) {
		$ratio = $width / $this->get_width();
		$height = $this->get_height() * $ratio;
		$this->resize($width, $height);
	}
   
	public function scale($scale) {
		$width = $this->get_width() * $scale/100;
		$height = $this->get_height() * $scale/100; 
		$this->resize($width, $height);
	}

	public function resize_max_width_max_height($width, $height) {
	    $ratio = $this->get_width()/ $this->get_height();
        if( $ratio > 1) {
            $width = $width;
            $height = $height/$ratio;
        }
        else {
            $width = $width * $ratio;
            $height = $height;
        }// var_dump($ratio,$width,$height);exit;
		$this->resize($width, $height);
	}
	
	public function resize($width,$height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->get_width(), $this->get_height());
		$this->image = $new_image;   
	}
}
?>
