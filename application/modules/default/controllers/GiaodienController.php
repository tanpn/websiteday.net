<?php
class GiaodienController extends Zend_Controller_Action{

    private $logger;
    private $product_bus;

    public function init() {

        $this->logger = Zend_Registry::get('logger');
        $this->product_bus = Product_Business::getInstance();
        $this->addCSS(STATIC_DOMAIN . '/css/flatsome-shop.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/style.css');
       	$this->addCSS(STATIC_DOMAIN . '/css/fl-icons.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/twocolumns.css');
      
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');
        
    }
    
    public function indexAction(){ 
       try { 
                $cond['category_id'] = '1';
                $cond['is_danh_sach'] = 't';
                $cond['enable_flag'] = 't';
        		$this->view->themes=$this->product_bus->findThemes($cond);
                $this->view->pageType = "giaodien";
                $data = $this->view->render("giaodien/index.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
           
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }

   
    public function chitietgiaodienAction(){ 
       try { 
                $id = $this->getRequest()->getParam("id", 0);
                $cond['enable_flag'] = 't';
                $cond['id'] = $id;
        		$this->view->themes=$this->product_bus->findThemes($cond);
                $this->view->pageType = "giaodien";
                $data = $this->view->render("giaodien/chitietgiaodien.phtml");
            } catch (Exception $e) {
               // var_dump($e);exit;
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
           
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }
   
    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }
}
