<?php

class SupportController extends Zend_Controller_Action {

     public function init() { 	
        $this->logger = Zend_Registry::get('logger');
        $this->product_bus = Product_Business::getInstance();
     
    	$this->addCSS(STATIC_DOMAIN . '/css/style.css');
		$this->addCSS(STATIC_DOMAIN . '/css/fl-icons.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome-shop.css');
      	
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');
    }

    public function quydinhchungAction() {
        try {
                $this->view->pageType = "quydinhchung";
                $key="chinh_sach_chung";
                $cond['key']=$key;
        		$this->view->keyValueData=$this->product_bus->findKeyValue($cond);
                $data = $this->view->render("support/quydinhchung.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->view->active_quydinhchung=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }


    public function vechungtoiAction() {
        try {
                $this->view->pageType = "vechungtoi";
                $key = "ve_chung_toi";
                $cond['key']=$key;
        		$this->view->keyValueData=$this->product_bus->findKeyValue($cond);
                $data = $this->view->render("support/vechungtoi.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->view->active_vechungtoi=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    public function hoptackinhdoanhAction() {
        try {
                $this->view->pageType = "hoptackinhdoanh";
                $key = "hop_tac_kinh_doanh";
                $cond['key']=$key;
        		$this->view->keyValueData=$this->product_bus->findKeyValue($cond);
                $data = $this->view->render("support/hoptackinhdoanh.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->view->active_vechungtoi=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    public function lienheAction() {
        try {
                $this->view->pageType = "bando";
                $data = $this->view->render("support/lienhe.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->view->active_map=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    public function emailsubcriberAction() {
        $insertedItemNumber = 0;
        try {
            if ($this->getRequest()->isPost()) {
                $email = $this->getRequest()->getParam("txtEmail", "");
                // Check valid
                if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$email))
                { 
                    echo "<script type='text/javascript'>top.emailSubcriber.alertEmailSubcriber('999');</script>";
                    exit;
                }
                $data = array(
                    "email" => $email,
                    "time_subcriber" => date('Y-m-d H:i:s',time())
                );
                // findEmailSubcriber
                $existedEmail = $this->product_bus->findEmailSubcriber($data);
                if(!empty($existedEmail)) {
                    // Email existed
                    echo "<script type='text/javascript'>top.emailSubcriber.alertEmailSubcriber('-1');</script>";
                    exit;
                }
                $insertedItemNumber = $this->product_bus->putEmailSubcriber($data);
            }
        } catch (Exception $e) {
            $insertedItemNumber = 0;
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }
        echo "<script type='text/javascript'>top.emailSubcriber.alertEmailSubcriber('".$insertedItemNumber."');</script>";
        exit;
    }

     public function privacyAction() {     
       
       
            try {
              
              
                $data = $this->view->render("support/privacy.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
       
        $this->view->content = $data;
    }
    public function termAction() {     
       
       
            try {
              
              
                $data = $this->view->render("support/term.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
       
        $this->view->content = $data;
    }
    public function contactAction() {
        try {
            if ($this->getRequest()->isPost()) {
                $data = array(
                    "person_contact" => $this->getRequest()->getParam("txtHoten", ""),
                    "role" => $this->getRequest()->getParam("txtChucvu", ""),
                    "company" => $this->getRequest()->getParam("txtCongty", ""),
                    "address" => $this->getRequest()->getParam("txtDiachi", ""),
                    "email" => $this->getRequest()->getParam("txtEmail", ""),
                    "website" => $this->getRequest()->getParam("txtWebsite", ""),
                    "phone" => $this->getRequest()->getParam("txtDienthoai", ""),
                    "mobile" => $this->getRequest()->getParam("txtDidong", ""),
                    "fax" => $this->getRequest()->getParam("txtFax", ""),
                    "content" => $this->getRequest()->getParam("txtNoidung", ""),
                    "time" => time()
                );
                $this->biz->putContact($data);
            }
            $id = rand(1, 100);
            $this->view->capchar = $this->biz->getCapchar($id);
            $this->view->content = $this->view->render("support/contact.phtml");
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function introAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/intro.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function agreeAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/agree.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function ruleAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/rule.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function changeprodAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/changeprod.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function promiseAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/promise.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function ptgiaonhanAction() {
        try {
            $key = $this->keycache;
            $data = $this->biz->getPage($key);
            if ($data == false) {
                $data = $this->view->render("support/ptgiaonhan.phtml");
                $this->biz->setPage($key, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function ptthanhtoanatmAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/ptthanhtoanatm.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function ptthanhtoancodAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/ptthanhtoancod.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function tichluyAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/tichluy.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function toptaikhoanAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/toptaikhoan.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function topmuahangAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/topmuahang.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function topthanhtoanAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/topthanhtoan.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function topgiaonhanAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/topgiaonhan.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function tophuydoiAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/tophuydoi.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function toptichluyAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/toptichluy.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function quychehdAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/quychehd.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function chinhsachAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/chinhsach.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function sitemapAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/sitemap.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function huongdanmuaAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/huongdanmua.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function hinhthucthanhtoanAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/hinhthucthanhtoan.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    public function topcauhoiAction() {
        try {
            $data = $this->biz->getPage($this->keycache);
            if ($data == false) {
                $data = $this->view->render("support/topcauhoi.phtml");
                $this->biz->setPage($this->keycache, $data);
            }
            $this->view->content = $data;
            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

	public function sendkhuyenmaiAction(){				
		header("Content-type: text/javascript");
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
		$list_email= "tanthang1986@yahoo.com,mice_vn@yahoo.com,cangnt@zing.vn,nickyvong@gmail.com,pickouvn@yahoo.com,achxi@zing.vn,moviefusion@gmail.com,ngay29@zing.vn,abadlygirl@yahoo.com,vannt2006@gmail.com,anan0479@yahoo.com.vn,ym2dung@yahoo.com,9dminhla@zing.vn,ngan_nhi@mail2world.com,wannabecool@zing.vn,chilinhnet@yahoo.com,heocon2110@zing.vn,tbuon25@yahoo.com,bedieudieu1983@yahoo.com.vn,bamboo851984@yahoo.com,edward.hwang@vinagame.com.vn,rubic2911@yahoo.com,tuvanhoa@yahoo.com,nhunt@vng.com.vn,tqhung1979@yahoo.com,nhi345@zing.vn,sarangkkoi@zing.vn,tinhocmo@zing.vn,emily05102003@gmail.com,dzobanbe@yahoo.vn,hiencv@vinagame.com.vn,kgbdoandang@yahoo.com,ruanyingjun@zing.vn,pig_bang@yahoo.com,doanngoc123@zing.vn,mimosagtim@yahoo.com,hellangelth@yahoo.com,vandl@vinagame.com.vn,thuonginc@zing.vn,uyenuyen13@yahoo.com,StarOfFate@yahoo.com,vudohuy@yahoo.com,khicontn@zing.vn,nguyenlong211288@yahoo.com,tungnvx@vinagame.com.vn,tnbp7@yahoo.com,truccv0912@yahoo.com,abc@abc.con,meoluoi83@gmail.com,belutrum@yahoo.com,mitsuki3010@yahoo.com,vitphuong2102@yahoo.com,meocon.nhoanh@yahoo.com,mtukhanh@yahoo.com,ki4t4s102@yahoo.com,hahuynhthanh@zing.vn,minhgiaoquanchua@yahoo.com,yukie_eye@yahoo.com,muitungnga@yahoo.com,le_nguyen0306@yahoo.ca,elegize2412@yahoo.com,mentranvg@gmail.com,queenbeau@zing.vn,mrcufamily@zing.vn,gacon282@zing.vn,thanhhienmt@zing.vn,zing.duongvt@gmail.com,mr.robbey@gmail.com,truongthaot3@yahoo.com,dzitconhaman@zing.vn,minhtrang0510@zing.vn,bunbun88@zing.vn,tnh_tinhkhucvang@yahoo.com,ga0_th3t_per_ti@yahoo.it,linuxpham@zing.vn,iamzonezone@zing.vn,andytuan85@gmail.com";
		$emails = explode(",",$list_email);
		foreach($emails as $email) 		
		{			
			//$email="tanthang1986@yahoo.com";
			Common::sendKhuyenMai($email); 
			echo $email."\n";
		}
		die("finished !");
	}

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
