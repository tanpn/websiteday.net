<?php

class HochiminhController extends Zend_Controller_Action {

    private $logger;
    private $keycache;
    private $biz;

    public function init() {   
   // echo $this->_request->getParam('id') ;exit;
        $this->logger = Zend_Registry::get('logger');
        $info = $this->getRequest()->getParams(); var_dump($info);exit;
        $this->keycache = $info['controller'] . '_' . $info['action'];
        $this->biz = Product_Business::getInstance();

        $auth = Session_Business::getInstance()->isLogin();
        if ($auth == false) {
            $this->view->islogin = false;
        } else {
            $this->view->auth = $auth;
            $this->view->islogin = true;
        }        

        $this->addJS(STATIC_DOMAIN . '/js/jquery.boxy.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js'); 
    }

    public function indexAction() {     
       
       
            try {
                //var_dump($this->biz->getDeal(1, 16,0));exit;
                $this->view->deal = $this->biz->getDeal(1, 16,0);
                
              
                $data = $this->view->render("index/index.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
       
        $this->view->content = $data;
    }

    public function contactAction() {
        try {
            if ($this->getRequest()->isPost()) {
                $data = array(
                    "person_contact" => $this->getRequest()->getParam("txtHoten", ""),
                    "role" => $this->getRequest()->getParam("txtChucvu", ""),
                    "company" => $this->getRequest()->getParam("txtCongty", ""),
                    "address" => $this->getRequest()->getParam("txtDiachi", ""),
                    "email" => $this->getRequest()->getParam("txtEmail", ""),
                    "website" => $this->getRequest()->getParam("txtWebsite", ""),
                    "phone" => $this->getRequest()->getParam("txtDienthoai", ""),
                    "mobile" => $this->getRequest()->getParam("txtDidong", ""),
                    "fax" => $this->getRequest()->getParam("txtFax", ""),
                    "content" => $this->getRequest()->getParam("txtNoidung", ""),
                    "time" => time()
                );
                $this->biz->putContact($data);
            }
            $id = rand(1, 100);
            $this->view->capchar = $this->biz->getCapchar($id);
            $this->view->content = $this->view->render("index/contact.phtml");

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
