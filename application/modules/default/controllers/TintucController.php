<?php
class TintucController extends Zend_Controller_Action{

    private $logger;
    private $product_bus;

    public function init() {
        $this->logger = Zend_Registry::get('logger');
        $this->product_bus = Product_Business::getInstance();
        $this->addCSS(STATIC_DOMAIN . '/css/flatsome-shop.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/style.css');
       	$this->addCSS(STATIC_DOMAIN . '/css/fl-icons.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/twocolumns.css');
      
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');
    }
    
    public function danhsachhoatdongAction(){ 
       try { 
                $this->view->pageType = "hoatdong";
                $cond['category_id'] = CATEGORY_ID_ACTIVITY;
                $cond['enable_flag'] = 't';
                $cond['is_danh_sach'] = 't';
        		$this->view->news=$this->product_bus->findNews($cond);
                $data = $this->view->render("tintuc/danhsachhoatdong.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }
   
   public function chitiethoatdongAction(){
       try {
                $this->view->pageType = "hoatdong";
                $id = $this->getRequest()->getParam("id", 0);
                
                $cond['id'] = $id;
                $cond['category_id'] = CATEGORY_ID_ACTIVITY;
                $cond['enable_flag'] = 't';
        		$this->view->news=$this->product_bus->findNews($cond);// var_dump($this->product_bus->findNews($cond));exit;
                $data = $this->view->render("tintuc/chitiethoatdong.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }

    public function chitietdichvuAction() {
       try {
                $this->view->pageType = "dichvu";
                $id = $this->getRequest()->getParam("id", 0);
                
                $cond['id'] = $id;
                $cond['category_id'] = CATEGORY_ID_SERVICE;
                $cond['enable_flag'] = 't';
        		$this->view->news=$this->product_bus->findNews($cond);//var_dump($this->product_bus->findNews($cond),$cond);exit;
                $data = $this->view->render("tintuc/chitietdichvu.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }
   
	public function chitietmeovatAction(){ 
       try {
                $this->view->pageType = "tintuc";
                $cond['category_id'] = TIPS_CATEGORY_ID;
                $cond['enable_flag'] = 't';
        		$this->view->news=$this->product_bus->findNews($cond);
                $data = $this->view->render("tintuc/meovat.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
   }

    public function chitiettintucAction(){ 
        var_dump($this->_request->getParam('id') );

      echo '<pre>';
      print_r($this->_request->getParams());
      echo '</pre>';

      $this->_helper->viewRenderer->setNoRender();exit;
   }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }
}
