<?php

class ChitietController extends Zend_Controller_Action {

    private $logger;
    private $keycache;
    private $biz;

    public function init() {   
   // echo $this->_request->getParam('id') ;exit;
        $this->logger = Zend_Registry::get('logger');
       
        $this->biz = Product_Business::getInstance();

        $auth = Session_Business::getInstance()->isLogin();
        if ($auth == false) {
            $this->view->islogin = false;
        } else {
            $this->view->auth = $auth;
            $this->view->islogin = true;
        }        

        $this->addJS(STATIC_DOMAIN . '/js/jquery.boxy.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js'); 
    }

    public function indexAction() {     
      Zend_Layout::startMvc(APPLICATION_PATH . 'layouts');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayoutPath(APPLICATION_PATH . 'layouts/')->setLayout('layout_detail');
       
            try {
                 $info = $this->getRequest()->getParams();
                 $id = Common::getIdFromUrl($info["id"]);
                //var_dump($this->biz->getDetailDeal($id));exit;
                 $detailInfo=$this->biz->getDetailDeal($id);
                $this->view->deal = $detailInfo;
            // $this->view->newDeal = $this->biz->getNewDeal($id,MAX_NEW_ON_DETAIL_PAGE);
              //$this->view->sameDeal = $this->biz->getSameDeal($id,$detailInfo['type'],MAX_SAME_DEAL_ON_DETAIL_PAGE); //var_dump($this->biz->getSameDeal($id,$detailInfo['type'],MAX_SAME_DEAL_ON_DETAIL_PAGE));exit;
                $data = $this->view->render("chitiet/index.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
       
        $this->view->content = $data;
    }

    public function contactAction() {
        try {
            if ($this->getRequest()->isPost()) {
                $data = array(
                    "person_contact" => $this->getRequest()->getParam("txtHoten", ""),
                    "role" => $this->getRequest()->getParam("txtChucvu", ""),
                    "company" => $this->getRequest()->getParam("txtCongty", ""),
                    "address" => $this->getRequest()->getParam("txtDiachi", ""),
                    "email" => $this->getRequest()->getParam("txtEmail", ""),
                    "website" => $this->getRequest()->getParam("txtWebsite", ""),
                    "phone" => $this->getRequest()->getParam("txtDienthoai", ""),
                    "mobile" => $this->getRequest()->getParam("txtDidong", ""),
                    "fax" => $this->getRequest()->getParam("txtFax", ""),
                    "content" => $this->getRequest()->getParam("txtNoidung", ""),
                    "time" => time()
                );
                $this->biz->putContact($data);
            }
            $id = rand(1, 100);
            $this->view->capchar = $this->biz->getCapchar($id);
            $this->view->content = $this->view->render("index/contact.phtml");

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
