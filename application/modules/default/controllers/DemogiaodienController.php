<?php
class DemogiaodienController extends Zend_Controller_Action{

    private $logger;


    public function init() {
        $this->logger = Zend_Registry::get('logger');
        /*Zend_Layout::startMvc(APPLICATION_PATH . 'layouts');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayoutPath(APPLICATION_PATH . 'layouts/')->setLayout('layout_empty');*/
        $this->_helper->_layout->setLayout('layout_empty');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
    }

    public function demogiaodienAction(){ 
       try { 
                $cond['id'] = $this->getRequest()->getParam("id", 0);
        		$this->view->themes = Product_Storages_GiaoDienImpl::getInstance()->findThemes($cond);
                $data = $this->view->render("demogiaodien/demogiaodien.phtml");// var_dump($data);exit;
            } catch (Exception $e) {
               // var_dump($e);exit;
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
           
        $this->view->content = $data;//var_dump($data);exit;
        $this->addCSS(BASE_URL . '/css/demo/index.spa.bundle.b6450689dd86380f67dc.css');
        $this->addCSS(BASE_URL . '/css/demo/page_livedemo.spa.chunk.b6450689dd86380f67dc.css');
        $this->addCSS(BASE_URL . '/css/demo/component_liveChat.spa.chunk.b6450689dd86380f67dc.css');
   }
   
    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }
}
