<?php

class IndexController extends Zend_Controller_Action {

    private $logger;
    private $product_bus;

    public function init() {
        $this->logger = Zend_Registry::get('logger');
        //$this->product_bus = Product_Business::getInstance();


      	//$this->addCSS(STATIC_DOMAIN . '/css/BQWrbCxwKTG.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/default.min.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome-shop.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/public.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/rouofWlolT7.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/settings.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/style.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/styles.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/twocolumns.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/WjdhTa51lbt.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/Xc7xVJSP4_r.css');
      	//$this->addCSS(STATIC_DOMAIN . '/css/YoRGRY8zOwk.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/fl-icons.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/flatsome.css?v=1');
      

      	
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');

       // $this->view->productTopBuyWeek=$this->product_bus->findProductTopWeek($cond);
        //$this->view->boxright = $this->view->render("box/boxright.phtml");
    }

	/**
	 * Index action for home page.
	 */
    public function indexAction() {
        try {
                $this->view->pageType = "homePage";
                // List product top week
        		//$this->view->productTopWeek=$this->product_bus->findProductTopWeek($cond);
                //$this->view->boxtopweek = $this->view->render("box/boxtopweek.phtml");
                //$this->view->boxSlider = $this->view->render("box/boxslidertop.phtml");
                // List all product
        		//$this->view->product=$this->product_bus->findProduct(null);
                $data = $this->view->render("index/index.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
                var_dump($e);exit;
            }
        
        //$this->view->boxright = $boxright;
        $this->view->content = $data;

        $this->view->active_homepage=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    /**
     * Shopping cart action.
     */
	public function cartAction() {
        $cart = $_COOKIE[SHOPPING_CART_COOKIE];
        $shopingCart=array();
        $strProductIds="";
        if (isset($cart) && strlen($cart) > 0) {
        	$productIds = explode("_", $cart);
         	$totalItemShoping = count($productIds);
           	for($i=0; $i< $totalItemShoping; $i++){
           		$key = $productIds[$i];
           		if(isset($shopingCart[$key])) {
           			$shopingCart[$key] = $shopingCart[$key] + 1;
           		} else {
           			$shopingCart[$key] = 1;
           			$strProductIds.=$key.",";
           		}
           	}
           	$strProductIds = substr($strProductIds,0,strlen($strProductIds)-1);
        }
        if(!empty($shopingCart)) {
        	$cond = array();
        	$cond['ids'] = $strProductIds;
        	$this->view->cart = $this->product_bus->findProduct($cond);
        	$this->view->quantity = $shopingCart;
        }

        try {
                $data = $this->view->render("product/cart.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->totalItemShoping = count($shopingCart);
        $this->view->content = $data;
        $this->addCSS(STATIC_DOMAIN . '/css/main_cart.css');
        $this->addCSS(STATIC_DOMAIN . '/css/default.css');
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    public function contactAction() {
        try {
            if ($this->getRequest()->isPost()) {
                $data = array(
                    "person_contact" => $this->getRequest()->getParam("txtHoten", ""),
                    "role" => $this->getRequest()->getParam("txtChucvu", ""),
                    "company" => $this->getRequest()->getParam("txtCongty", ""),
                    "address" => $this->getRequest()->getParam("txtDiachi", ""),
                    "email" => $this->getRequest()->getParam("txtEmail", ""),
                    "website" => $this->getRequest()->getParam("txtWebsite", ""),
                    "phone" => $this->getRequest()->getParam("txtDienthoai", ""),
                    "mobile" => $this->getRequest()->getParam("txtDidong", ""),
                    "fax" => $this->getRequest()->getParam("txtFax", ""),
                    "content" => $this->getRequest()->getParam("txtNoidung", ""),
                    "time" => time()
                );
                $this->biz->putContact($data);
            }
            $id = rand(1, 100);
            //$this->view->capchar = $this->biz->getCapchar($id);
            $this->view->content = $this->view->render("index/contact.phtml");

            $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
    }

    /**
	 * Test upload form.
	 */
    public function uploadformAction() {
        try {
                
                $data = $this->view->render("index/uploadform.phtml");
               // var_dump('abc',$data);exit;
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;

        $this->view->active_homepage=ACTIVE_MENU_CLASS;
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

    /**
	 * Test upload file.
	 */
    public function uploadAction() {
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       header("Content-type:text/javascript");

       $image_path_upload = $_SERVER['DOCUMENT_ROOT']."/images/upload/product/". date('Ymd');
       $temp_path_upload = $_SERVER['DOCUMENT_ROOT']."/images/upload/product". "/tmp/" . date('Ymd');
       if (!file_exists ($image_path_upload) || !is_dir( $image_path_upload) ) {
            mkdir( $image_path_upload, 0755, true );       
       }
       if (!file_exists ($temp_path_upload) || !is_dir( $temp_path_upload) ) {
            mkdir( $temp_path_upload, 0755, true );       
       }

        $target_file = $temp_path_upload . "/". time() .  "_" .basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        } 
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                
                $image = new ResizeImage();
	
            	$image->init($target_file);
            	
                $arr_size[0]['w'] = 250;
                $arr_size[0]['h'] = 250;
                $arr_size[1]['w'] = 400;
                $arr_size[1]['h'] = 250;
                $arr_size[2]['w'] = 600;
                $arr_size[2]['h'] = 800;
                $arr_size[3]['w'] = 1016;
                $arr_size[3]['h'] = 940;
                
                //$file_name = "hinh-test";
                $file_name =  md5(time());
                $date = date('Ymd');
                foreach($arr_size as $size) {
                    $image->init($target_file);
                    $file_dest = $image_path_upload . "/" . $file_name ."-". $size['w'] . "x" . $size['h']. ".jpg";
                    $image->resize_max_width_max_height($size['w'], $size['h']);
            	    $image->save($file_dest);
                }

                $array_response = array(
                    "date" => $date,
                    "file_name" => $file_name
                );
                // Convert Array to JSON String
                $string_json_response = json_encode($array_response);
            	
            	echo $string_json_response;
                
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

class ResizeImage {
   
	private $image;
	private $image_type;
 
	public function init($filename) {
		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		
		switch($this->image_type) {
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($filename);
				break;
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($filename);
				break;
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($filename);
				break;
		}
	}
	
	public function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
		switch($image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image, $filename, $compression);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image,$filename);
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image,$filename);
				break;
			default:
				return false;
				break;
		}
		
		if($permissions != null) {
			chmod($filename, $permissions);
		}
		
		return true;
	}
	public function output($image_type = IMAGETYPE_JPEG) {
		switch($image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image); 
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image);
				break;
		}  
	}
	public function get_width() {
		return imagesx($this->image);
	}
   
	public function get_height() {
		return imagesy($this->image);
	}
	
	public function resize_to_height($height) {
		$ratio = $height / $this->get_height();
		$width = $this->get_width() * $ratio;
		$this->resize($width, $height);
	}
   
	public function resize_to_width($width) {
		$ratio = $width / $this->get_width();
		$height = $this->get_height() * $ratio;
		$this->resize($width, $height);
	}
   
	public function scale($scale) {
		$width = $this->get_width() * $scale/100;
		$height = $this->get_height() * $scale/100; 
		$this->resize($width, $height);
	}

	public function resize_max_width_max_height($width, $height) {
	    $ratio = $this->get_width()/ $this->get_height();
        if( $ratio > 1) {
            $width = $width;
            $height = $height/$ratio;
        }
        else {
            $width = $width * $ratio;
            $height = $height;
        }// var_dump($ratio,$width,$height);exit;
		$this->resize($width, $height);
	}
	
	public function resize($width,$height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->get_width(), $this->get_height());
		$this->image = $new_image;   
	}
}

?>
