<?php

class PopupController extends Zend_Controller_Action {

    private $logger;
    private $keycache;
    private $biz;

    public function init() {
        $this->logger = Zend_Registry::get('logger');
        $info = $this->getRequest()->getParams();
        $this->keycache = $info['controller'] . '_' . $info['action'];
        $this->biz = Product_Business::getInstance();
         $this->addJS(STATIC_DOMAIN.'/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN.'/js/api_'.API_JS_VERSION.'.js');
    }

      
    public function messageAction() { 
        try {               
            if($this->getRequest()->isPost()){
                $data=array(
                    'namekh'=>$this->getRequest()->getParam("namekh",""),
                    'phonekh'=>$this->getRequest()->getParam("phonekh",""),
                    'emailkh'=>$this->getRequest()->getParam("emailkh",""),
                    'ctmsg'=>$this->getRequest()->getParam("ctmsg",""),
                    'prodid'=>$this->getRequest()->getParam("prodid",0),
                    'time'=>time()
                );
                $this->biz->putMessage($data);
            }
            $content= $this->view->render("popup/message.phtml");
            echo $content;            
            exit;
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN.'/js/api_'.API_JS_VERSION.'.js');
          $this->addJS(STATIC_DOMAIN.'/js/jquery.boxy.js');
        $this->addJS(STATIC_DOMAIN.'/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN.'/js/jquery.lightbox-0.5.pack.js');
         
    }
    
    public function clickAction(){
        header("Content-type: text/javascript");
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->logger = Zend_Registry::get('logger');
        $err = 0;
        try {
            $id = $this->_request->getParam('id', 0);
            if($id>0)
                $this->biz->click($id);
        } catch (Exception $e) {
            $err = 1;
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }
        $arr = array('err' => $err);
        $strJSON = $_GET['callback'];
        $strJSON.="(";
        $strJSON.=json_encode($arr);
        $strJSON.=")";
        echo $strJSON;
    }
    
    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
