<?php

class CategoryController extends Zend_Controller_Action {

    private $logger;
    private $product_bus;

    public function init() { 	
        $this->logger = Zend_Registry::get('logger');
        $this->product_bus = Product_Business::getInstance();
        $this->addCSS(STATIC_DOMAIN . '/css/tet.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/bootstrap.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/bootstrap_002.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/reset.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/templates.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/modules.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/product_1.00.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/menu.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/block_productsselect.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/block_showcart_1.00.css');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');

        $this->view->productTopBuyWeek=$this->product_bus->findProductTopBuyWeek($cond);
        $boxright = $this->view->render("box/boxright.phtml");
        $this->view->boxright = $boxright;
    }

    public function indexAction() {
        try {
                $cate_id = $this->getRequest()->getParam("cateid", 0);
                $cond['cate_id']=$cate_id;
                $this->view->product = $this->product_bus->findProductByCategory($cond);
                $category = $this->product_bus->findCategory($cond)[0];
        		$this->view->category =$category;
        		$cond['cate_id']=$category["parent_cate_id"];
        		$this->view->parent_category = $this->product_bus->findCategory($cond)[0];
        		
        		$this->view->cateId = $cate_id;
        		$this->view->pageType = "category";
                $data = $this->view->render("product/cate.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->content = $data;
        $this->view->active_listall=ACTIVE_MENU_CLASS;
    }

    public function index2Action() {
        try {
            $id = $this->getRequest()->getParam("id", 0);
            $page = $this->getRequest()->getParam("page", 1);
            $name = $this->getRequest()->getParam("name","");
            $key=$this->keycache."_".$id."_".$page;
            $data = $this->biz->getPage($this->keycache);
            //if ($data == false) 
            {
                $this->view->name=$name;
                $this->view->page =$page;
                $this->view->cat =$id;
                $this->view->total = $this->biz->getTotalProByRootCat($id);
                $this->view->product=$this->biz->getProByRootCat($id,$page,MAX_ITEM_PER_PAGE);
                $this->view->subcat=$this->biz->getSubCat($id);
                $data = $this->view->render("category/index.phtml");
                $this->biz->setPage($key, $data);
            }
            $supcat=array(
                'id'=>$id,
                'name'=>$name
            );
            //var_dump($id,$this->biz->getSupCat($id));exit;
            $this->view->supcat=$supcat;
            $this->view->content = $data;
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
    }

    public function detailAction() {
        try {
            $product_id = $this->getRequest()->getParam("id", 0);
            $cat = $this->getRequest()->getParam("cat", 0);
            $key = $this->keycache . $product_id . $cat;
            $data = $this->biz->getPage($key);
            //if ($data == false) 
            {
                $info = $this->biz->getInfo($product_id);
                if (is_numeric($cat) == true) {
                    $same = $this->biz->getProductByCategory($cat, ID_SAME_KIND, MAX_ITEM_SAME_KIND);
                } else {
                    if (strcmp($cat, "saleoff") == 0) {
                        $same = $this->biz->getSaleOffProduct(ID_SAME_KIND, MAX_ITEM_SAME_KIND);
                    } else {
                        if (strcmp($cat, "focus") == 0) {
                            $same = $this->biz->getFocusProduct(ID_SAME_KIND, MAX_ITEM_SAME_KIND);
                        } else {
                            if (strcmp($cat, "new") == 0) {
                                $same = $this->biz->getNew(ID_SAME_KIND, MAX_ITEM_SAME_KIND);
                            }
                        }
                    }
                }
                $this->view->cat = $cat;
                $this->view->name = $this->getRequest()->getParam("name", "");
                $this->view->info = $info;
                $this->view->same = $same;
                $data = $this->view->render("product/detail.phtml");
                $this->biz->setPage($key, $data);
            }
            $this->view->content = $data;
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
//        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.jqzoom.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
        
        $this->addJS(STATIC_DOMAIN . '/js/jquery.jqzoom-core.js');
        
        
    }

    public function searchAction() {
        try {
            $kw = $this->getRequest()->getParam("kw", "");
            $page = $this->getRequest()->getParam("page", 1);
            $total = $this->getRequest()->getParam("total", 0);
            $product = $this->biz->searchProduct($kw, $page, MAX_ITEM_PER_PAGE);
            if ($total == 0)
                $total = $this->biz->searchTotal($kw);
            $this->view->product = $product;
            $this->view->total = $total;
            $this->view->page = $page;
            $this->view->name = $kw;
            $this->view->content = $this->view->render("product/search.phtml");
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
    }

    public function checkoutAction() {
        try {
            $selprod = $_COOKIE[SEL_PRODUCT];
            $flgcheckout = $_COOKIE["flgcheckout"];
            if (!isset($selprod) || !isset($flgcheckout) || $flgcheckout == 0) {
                header("location:" . BASE_URL . "/product/card");
                exit;
            }
            if ($this->getRequest()->isPost()) {
                $uname = "";
                $time = time();
                $auth = Session_Business::getInstance()->isLogin();
                if ($auth != false) {
                    $uname = $auth["uname"];
                }
                $data = array(
                    'id' => $_COOKIE[SEL_PRODUCT],
                    'name' => $this->getRequest()->getParam("namekh", ""),
                    'phone' => $this->getRequest()->getParam("phonekh", ""),
                    'email' => $this->getRequest()->getParam("emailkh", ""),
                    'address' => $this->getRequest()->getParam("addresskh", ""),
                    'province' => $this->getRequest()->getParam("tinh", ""),
                    'note' => $this->getRequest()->getParam("notekh", ""),
                    'credit' => 0,
                    'verify' => 0,
                    'uname' => $uname,
                    'time' => $time,
                    'status' => 0,
                    'amount' => $this->getRequest()->getParam("amount", 0)
                );
                $this->biz->putCard($data);
                setcookie("flgcheckout", "0");
            }
            $this->view->time = $time;
            $this->view->content = $this->view->render("product/checkout.phtml");
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }

    public function successAction() {
        try {
            if ($this->getRequest()->isPost()) {
                $selpay = $this->getRequest()->getParam("select_pay"); var_dump($selpay);exit;
                $time = $this->getRequest()->getParam("time");
                $id = $_COOKIE[SEL_PRODUCT];
                if (isset($id) && isset($time) && is_numeric($time)) {
                    $data = array(
                        'id' => $id,
                        'credit' => $selpay
                    );
                    $this->biz->updateCheckout($data,$time);
                    setcookie(SEL_PRODUCT, "", -1);
                    //send email
                    $invoice = $this->biz->getCard($time);
                    try {
                        Common::sendInvoiceMail($invoice);
                    } catch (Exception $ex) {
                        
                    }
                }
            }
            $key = $this->keycache;
            $data = $this->biz->getPage($key);
            if ($data == false) {
                $data = $this->view->render("product/success.phtml");
                $this->biz->setPage($key, $data);
            }
            $this->view->content = $data;
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }

    private function checkExisted($arr, $item) {
        foreach ($arr as $it) {
            if ($it == $item)
                return true;
        }
        return false;
    }

    public function cardAction() {
        try {
            $pro = array();
            $ids = array();
            $selprod = $_COOKIE[SEL_PRODUCT];
            if (isset($selprod)) {
                $selprod = base64_decode($selprod);
                $check = explode("-", $selprod);
                if (count($check) > 1 && strlen($check[0]) > 0 && strlen($check[1]) > 0) {
                    if ($this->getRequest()->isPost()) {
                        $selprod = explode(",", $selprod);
                        $data = "";
                        foreach ($selprod as $item) {
                            $tmp = explode("-", $item);
                            $quantity = $this->getRequest()->getParam("quantity_" . $tmp[0]);
                            $data.=$tmp[0] . "-" . $quantity . ",";
                            $ids[] = intval($tmp[0]);
                            $info = $this->biz->getInfo(intval($tmp[0]));
                            $info["quantity"] = intval($quantity);
                            $pro[] = $info;
                        }
                    } else {
                        $selprod = explode(",", $selprod);
                        foreach ($selprod as $item) {
                            $tmp = explode("-", $item);
                            if (!$this->checkExisted($ids, $tmp[0])) {
                                $ids[] = intval($tmp[0]);
                                $info = $this->biz->getInfo(intval($tmp[0]));
                                $info["quantity"] = intval($tmp[1]);
                                $pro[] = $info;
                            } else {
                                foreach ($pro as &$it) {
                                    if ($it["id"] == $tmp[0]) {
                                        $it["quantity"]+=intval($tmp[1]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $selprod = "";
            $n = count($pro);
            for ($i = 0; $i < $n - 1; $i++) {
                $item = $pro[$i];
                $selprod.=$item["id"] . "-" . $item["quantity"] . ",";
            }
            if ($n > 0) {
                $selprod.=$pro[$n - 1]["id"] . "-" . $pro[$n - 1]["quantity"];
                $selprod = base64_encode($selprod);
                setcookie(SEL_PRODUCT, $selprod);
                $id = rand(1, 100);
                $this->view->capchar = $this->biz->getCapchar($id);
            } else {
                setcookie(SEL_PRODUCT, $selprod, -1);
            }
            $this->view->card = $pro;
            $this->view->content = $this->view->render("product/card.phtml");
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }

    public function sumcardAction() {
        header("Content-type: text/javascript");
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $ids = array();
        try {
            $key = SEL_PRODUCT;
            $selprod = $_COOKIE[$key];
            if (isset($selprod)) {
                $selprod = base64_decode($selprod);
                $check = explode("-", $selprod);
                if (count($check) > 1 && strlen($check[0]) > 0 && strlen($check[1]) > 0) {
                    $selprod = explode(",", $selprod);
                    foreach ($selprod as $item) {
                        $tmp = explode("-", $item);
                        if (!$this->checkExisted($ids, $tmp[0])) {
                            $ids[] = intval($tmp[0]);
                        }
                    }
                }
            }
            $total = count($ids);
            $arr = array('total' => $total);
            $strJSON = $_GET['callback'];
            $strJSON.="(";
            $strJSON.=json_encode($arr);
            $strJSON.=")";
            echo $strJSON;
            exit;
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }
    }

    public function historyAction() {
        try {
            if ($this->isLogin == true) {
                $page = $this->getRequest()->getParam('page', 1);
                $card = $this->biz->getHistory($this->auth['uname'], $page);
                $this->view->card = $card;
                $this->view->content = $this->view->render("product/history.phtml");
            } else {
                header("location:" . BASE_URL);
            }
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }

    public function clickAction() {
        header("Content-type: text/javascript");
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->logger = Zend_Registry::get('logger');
        $err = 0;
        try {
            $id = $this->_request->getParam('id', 0);
            if ($id > 0)
                $this->biz->click($id);
        } catch (Exception $e) {
            $err = 1;
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }
        $arr = array('err' => $err);
        $strJSON = $_GET['callback'];
        $strJSON.="(";
        $strJSON.=json_encode($arr);
        $strJSON.=")";
        echo $strJSON;
    }

    public function invoiceAction() {
        try {
            $pro = array();
            $ids = array();
            $selprod = $_COOKIE[SEL_PRODUCT];
            if (isset($selprod)) {
                $selprod = base64_decode($selprod);
                $check = explode("-", $selprod);
                if (count($check) > 1 && strlen($check[0]) > 0 && strlen($check[1]) > 0) {
                    if ($this->getRequest()->isPost()) {
                        $selprod = explode(",", $selprod);
                        $data = "";
                        foreach ($selprod as $item) {
                            $tmp = explode("-", $item);
                            $quantity = $this->getRequest()->getParam("quantity_" . $tmp[0]);
                            $data.=$tmp[0] . "-" . $quantity . ",";
                            $ids[] = intval($tmp[0]);
                            $info = $this->biz->getInfo(intval($tmp[0]));
                            $info["quantity"] = intval($quantity);
                            $pro[] = $info;
                        }
                    } else {
                        $selprod = explode(",", $selprod);
                        foreach ($selprod as $item) {
                            $tmp = explode("-", $item);
                            if (!$this->checkExisted($ids, $tmp[0])) {
                                $ids[] = intval($tmp[0]);
                                $info = $this->biz->getInfo(intval($tmp[0]));
                                $info["quantity"] = intval($tmp[1]);
                                $pro[] = $info;
                            } else {
                                foreach ($pro as &$it) {
                                    if ($it["id"] == $tmp[0]) {
                                        $it["quantity"]+=intval($tmp[1]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $selprod = "";
            $n = count($pro);
            for ($i = 0; $i < $n - 1; $i++) {
                $item = $pro[$i];
                $selprod.=$item["id"] . "-" . $item["quantity"] . ",";
            }
            if ($n > 0) {
                $selprod.=$pro[$n - 1]["id"] . "-" . $pro[$n - 1]["quantity"];
                $selprod = base64_encode($selprod);
                setcookie(SEL_PRODUCT, $selprod);
                $id = rand(1, 100);
                $this->view->capchar = $this->biz->getCapchar($id);
            } else {
                setcookie(SEL_PRODUCT, $selprod, -1);
            }
            $this->view->card = $pro;
            $this->view->content = $this->view->render("product/invoice.phtml");
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }
    
    public function recoverpassAction() {
        try {
            $this->view->content = $this->view->render("product/recoverpass.phtml");
        } catch (Exception $e) {
            $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
        }

        $this->addCSS(STATIC_DOMAIN . '/css/style_' . CSS_MAIN_STYLE_VERSION . '.css');
        $this->addCSS(STATIC_DOMAIN . '/css/jquery.lightbox-0.5.css');

        $this->addJS(STATIC_DOMAIN . '/js/api_' . API_JS_VERSION . '.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-1.4.2.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.lightbox-0.5.pack.js');
    }
    
    protected function addCSS($css) {
        $this->cssLinks[] = $css;
    }

    protected function addJS($js) {
        $this->jsLinks[] = $js;
    }

    public function dispatch($action) {
        try {
            parent::dispatch($action);
        } catch (Exception $ex) {
            if ($ex->getCode() != 404) {
                
            }
            return;
        }
        $this->view->currentAction = $action;
        $this->view->cssLinks = $this->cssLinks;
        $this->view->jsLinks = $this->jsLinks;
    }

}

?>
