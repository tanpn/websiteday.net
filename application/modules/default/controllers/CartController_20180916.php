<?php

class CartController extends Zend_Controller_Action {

    private $logger;
    private $product_bus;

    public function init() {
        $this->logger = Zend_Registry::get('logger');
        $this->product_bus = Product_Business::getInstance();

      	$this->addCSS(STATIC_DOMAIN . '/css/tet.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/bootstrap.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/bootstrap_002.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/reset.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/templates.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/modules.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/product_1.00.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/menu.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/block_productsselect.css');
      	$this->addCSS(STATIC_DOMAIN . '/css/block_showcart.css');
        $this->addJS(STATIC_DOMAIN . '/js/jquery-3.3.1.min.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.easing.js');
        $this->addJS(STATIC_DOMAIN . '/js/jquery.cookie.js');

        $this->view->productTopBuyWeek=$this->product_bus->findProductTopBuyWeek($cond);
        $boxright = $this->view->render("box/boxright.phtml");
        $this->view->boxright = $boxright;
    }

	/**
     * Shopping cart action.
     */
	public function cartAction() {
	    $this->view->pageType = "giohang";
        $cart = $_COOKIE[SHOPPING_CART_COOKIE];
        $shopingCart=array();
        $strProductIds="";
        if (isset($cart) && strlen($cart) > 0) {
        	$productIds = explode("_", $cart);
         	$totalItemShoping = count($productIds);
           	for($i=0; $i< $totalItemShoping; $i++){
           	    if(!empty($productIds[$i])){
           	    $key = $productIds[$i];
           		    if(isset($shopingCart[$key])) {
           			    $shopingCart[$key] = $shopingCart[$key] + 1;
               		} else {
               			$shopingCart[$key] = 1;
               			$strProductIds.=$key.",";
               		}
           	    }
           	}
           	$strProductIds = substr($strProductIds,0,strlen($strProductIds)-1);
        }
        if(!empty($shopingCart)) {
        	$cond = array();
        	$cond['ids'] = $strProductIds;
        	$this->view->cart = $this->product_bus->findProduct($cond);
        	$this->view->quantity = $shopingCart;
        }

        try {
                $data = $this->view->render("cart/cart.phtml");
            } catch (Exception $e) {
                $this->logger->log("File: " . $e->getFile() . ", Line : " . $e->getLine() . ",err: " . $e->getMessage(), Zend_Log::ERR);
            }
        $this->view->totalItemShoping = count($shopingCart);
        $this->view->content = $data;
        $this->addCSS(STATIC_DOMAIN . '/css/main_cart.css');
        $this->addCSS(STATIC_DOMAIN . '/css/default.css');
        $this->addJS(JS_URL . '/dacsanday_'.JS_VERSION.'.js');
    }

	/**
     * Shopping cart action.
     */
	public function thanhtoanAction() {
		// Write cart
		try{
	 	if ($this->getRequest()->isPost()) {
                $data = array(
                    "buyer_phone" => $this->getRequest()->getParam("buyer_phone", ""),
                    "buyer_address" => $this->getRequest()->getParam("buyer_address", ""),
                    "buyer_fullname" => $this->getRequest()->getParam("buyer_fullname", ""),
                    "buyer_email" => $this->getRequest()->getParam("buyer_email", ""),
                    "buyer_note" => $this->getRequest()->getParam("buyer_note", "Kh么ng c贸"),
                    "buyer_province" => $this->getRequest()->getParam("buyer_province", ""),
                    "order_date" => date('Y-m-d H:i:s',time()),
                    "fee_ship" => $this->getRequest()->getParam("fee_ship", ""),
                    "status_id" => 1
                );

                $this->product_bus->putCart($data);

                // Get order id of cart
                $cartInserted =$this->product_bus->findCartByDate($data['order_date']);
                if(!empty($cartInserted)) {
                    $orderId = $cartInserted[0]['order_id'];

            	 	// Write cart detail
                    $cart = $_COOKIE[SHOPPING_CART_COOKIE];
                    $shopingCart=array();
                    $strProductIds="";
                    if (isset($cart) && strlen($cart) > 0) {
                    	$productIds = explode("_", $cart);
                     	$totalItemShoping = count($productIds);
                       	for($i=0; $i< $totalItemShoping; $i++){
                       	    if(!empty($productIds[$i])){
                           		$key = $productIds[$i];
                           		if(isset($shopingCart[$key])) {
                           			$shopingCart[$key] = $shopingCart[$key] + 1;
                           		} else {
                           			$shopingCart[$key] = 1;
                           			$strProductIds.=$key.",";
                           		}
                       	    }
                       	}
                       	$strProductIds = substr($strProductIds,0,strlen($strProductIds)-1);
                    }
                    if(!empty($shopingCart)) {
                    	$cond = array();
                    	$cond['ids'] = $strProductIds;
                    	$product = $this->product_bus->findProduct($cond);
                    	//echo "<pre>";var_dump($product,$shopingCart );exit;
                    	$productDetailCart = array();
                    	$listProductInvoice =array();
                    	$totalInvoice =0;
                    	if(!empty($product)) {
                    		for($i=0;$i<count($product);$i++){
                    		    $productId = $product[$i]['id'];
                    		    $itemData = array(
                                    "order_id" => $orderId,
                                    "product_id" => $productId,
                                	"price" => $product[$i]['price'],
                    		    	"new_price" => $product[$i]['new_price'],
                                	"quantity" => $shopingCart[$productId],
                                    "status_id" => 1
                                );
                                $this->product_bus->putCartDetail($itemData);

                                $itemInvoice['id']=  $productId;
                                $itemInvoice['name']=  $product[$i]['name'];
                                $itemInvoice['price']=  $product[$i]['price'];
                                $itemInvoice['new_price']=  $product[$i]['new_price'];
                                $itemInvoice['quantity']=  $shopingCart[$productId];
                                $itemInvoice['sub_total']=  $product[$i]['new_price'] * $shopingCart[$productId];
                                $totalInvoice += $itemInvoice['sub_total'];
                                $listProductInvoice[] = $itemInvoice;
                    		}
                    	}
                    	/*
                    	// Max fee ship
                    	$maxFeeShip = 0;
                    	$arrFeeShip = explode("~", $data['fee_ship']);
                        if(count($arrFeeShip) == 1) {
                            $maxFeeShip = str_replace(".","",$arrFeeShip[0]);
                        }
                        if(count($arrFeeShip) == 2) {
                            $maxFeeShip = str_replace(".","",$arrFeeShip[1]);
                        }
                        var_dump($arrFeeShip,$maxFeeShip);exit;*/
                    	//Send email
                        $invoice['id']=$orderId;
                    	$invoice['time'] = $data['order_date'];
                    	$invoice['name'] = $data['buyer_fullname'];
                    	$invoice['address'] = $data['buyer_address'];
                    	$invoice['phone'] = $data['buyer_phone'];
                    	$invoice['email']=$data['buyer_email'];
                    	$invoice['note']=$data['buyer_note'];
                    	$invoice['list_product'] =$listProductInvoice;
                    	// Tien hang
                    	$invoice['total_invoice'] = $totalInvoice;
                    	// Phi ship
                    	$invoice['fee_ship'] = $data['fee_ship'];
                    	Common::sendInvoiceMail($invoice);
                    	//Delete cookie cart
                    	setcookie(SHOPPING_CART_COOKIE, "", time() - 3600,"/");
                    }
                }
        }

		} catch (Exception $e) {
            die ("C贸 l峄梚 x岷 ra, vui l貌ng th峄