<?php

define('BASE_PATH', realpath(dirname(__FILE__))); //public folder 
define('ROOT_PATH', realpath(dirname(__FILE__) )); //base folder
define('BASE_URL', 'https://' . $_SERVER['SERVER_NAME']);
define('APPLICATION_PATH', ROOT_PATH . '/application/');
define('LIBS_PATH', ROOT_PATH . '/Lib/');
define('CONFIGURATION_PATH', APPLICATION_PATH . '/etc/');
define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('APP_ENV', "development");

date_default_timezone_set('Asia/Ho_Chi_Minh');

set_include_path(LIBS_PATH . PATH_SEPARATOR . get_include_path());
error_reporting(E_ALL ^ E_NOTICE);

require_once "Zend/Loader.php"; 
@Zend_Loader::registerAutoload(); 

$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/etc/' . APP_ENV . '.global.ini', APP_ENV);
Zend_Registry::set('configuration', $configuration);

$logger = new Zend_Log();
$logger->addWriter(new Zend_Log_Writer_Stream($configuration->log->path));

Zend_Registry::set('logger', $logger);

try {
    require APPLICATION_PATH . '/bootstrap.php';
} catch (Exception $exception) {
    $logger->log("File: " . $exception->getFile() . ", Line : " . $exception->getLine() . ",err: " . $exception->getMessage(), Zend_Log::ERR);
    echo '<html><body><center>'
    . 'An exception occured while bootstrapping the application.';
    echo '<br /><br />' . $exception->getMessage() . '<br />'
    . '<div align="left">Stack Trace:'
    . '<pre>' . $exception->getTraceAsString() . '</pre></div>';
    echo '</center></body></html>';
    exit(1);
}

Zend_Controller_Front::getInstance()->dispatch();
?>
