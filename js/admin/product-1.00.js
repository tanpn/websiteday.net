var adminProduct = {
    IMAGE_URL : "",
    IMAGE_CURRENT_UPLOAD_ID:"",
    IMAGE_ROW : 0,
    IMAGE_ROW_BANNER : 0,
    UPLOAD_PRODUCT : 0,
    UPLOAD_BANNER : 1,
    initImageUploadPath : function (imageUrl) {
        adminProduct.IMAGE_URL = imageUrl;
    },
    
    openUploadThumb : function (id) {
        $('#imgupload').val('');
        adminProduct.IMAGE_CURRENT_UPLOAD_ID = id;
        $('#imgupload').trigger('click');
    },
    
    initUploadBaseOnChangeFile : function () {
         $('#imgupload').change(function(){
           $("#form_upload").submit();
        });
    },
    
    setImageDataUpload : function (data) {
        var retData = JSON.parse(data);
        var uploadType = retData['upload_type'];
        var imgTempFilePath = '';
        if(uploadType == adminProduct.UPLOAD_PRODUCT) {
            imgTempFilePath = adminProduct.IMAGE_URL + '/upload/product/' + retData['date'] + '/' + retData['file_name'] + '-100x100' + '.jpg';
            $('#'+adminProduct.IMAGE_CURRENT_UPLOAD_ID).attr('src', imgTempFilePath);
            $('#'+adminProduct.IMAGE_CURRENT_UPLOAD_ID + '_hidden').val(retData['date'] + '/' + retData['file_name']);
        } else if(uploadType == adminProduct.UPLOAD_BANNER) {
            imgTempFilePath = adminProduct.IMAGE_URL + '/upload/banner/' + retData['date'] + '/' + retData['file_name'] + '-100x100' + '.jpg';
            $('#'+adminProduct.IMAGE_CURRENT_UPLOAD_ID).attr('src', imgTempFilePath);
            $('#'+adminProduct.IMAGE_CURRENT_UPLOAD_ID + '_hidden').val(retData['date'] + '/' + retData['file_name']);
        }
    },

    removeUploadThumb : function (id) {
        $('#'+id).attr('src', adminProduct.IMAGE_URL + '/upload/product/no_image-100x100.png');
        $('#'+id + '_hidden').val('');
        $('#imgupload').val('');
    },
    
    addImage : function () {
            html = '<tr id="image-row' + adminProduct.IMAGE_ROW + '">';
             html += '   <td class="text-left">';
             html += '      <a href="javascript:void(0);" class="img-thumbnail"><img src="' + adminProduct.IMAGE_URL + '/upload/product/no_image-100x100.png' + '" id="image-thumb' + adminProduct.IMAGE_ROW + '"></a>';
             html += '      <div class="popover-content">';
             html += '         <button type="button" id="button-image" onclick="adminProduct.openUploadThumb(\'image-thumb' + adminProduct.IMAGE_ROW +'\');" class="btn btn-primary">';
             html += '         <i class="fa fa-pencil"></i></button>';
              html += '        <button type="button" id="button-clear" class="btn btn-danger" onclick="adminProduct.removeUploadThumb(\'image-thumb' + adminProduct.IMAGE_ROW +'\');"><i class="fa fa-trash-o"></i></button>';
             html += '      </div>';
             html += '      <input type="hidden" name="product_image[' + adminProduct.IMAGE_ROW + '][image]" value="" id="image-thumb' + adminProduct.IMAGE_ROW + '_hidden">';
             html += '   </td>';
             html += '   <td class="text-right"><input type="text" name="product_image[' + adminProduct.IMAGE_ROW + '][sort_order]" value="" placeholder="Thứ tự" class="form-control"></td>';
             html += '   <td class="text-left"><button type="button" onclick="$(\'#image-row' + adminProduct.IMAGE_ROW + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Loại bỏ"><i class="fa fa-minus-circle"></i></button></td>';
             html += '</tr>';

            $('#images tbody').append(html);

            adminProduct.IMAGE_ROW++;
    },
    
    addImageBanner : function () {
        html = '<tr id="image-row' + adminProduct.IMAGE_ROW_BANNER + '">';
        html += '<td class="text-left"><input type="text" name="banner_image[2][' + adminProduct.IMAGE_ROW_BANNER + '][title]" value="" placeholder="Tiêu đề:" class="form-control"></td>';
        html += '<td class="text-left" style="width: 30%;"><input type="text" name="banner_image[2][' + adminProduct.IMAGE_ROW_BANNER + '][link]" value="" placeholder="Liên kết" class="form-control"></td>';
        html += '<td class="text-center">';
        html += '<a href="" id="thumb-image' + adminProduct.IMAGE_ROW_BANNER + '" data-toggle="image" class="img-thumbnail"><img src="' + adminProduct.IMAGE_URL + '/upload/product/no_image-100x100.png' + '" id="image-thumb' + adminProduct.IMAGE_ROW_BANNER + '"></a>';
        html += '<div class="popover-content">         <button type="button" id="button-image" onclick="adminProduct.openUploadThumb(\'image-thumb' + adminProduct.IMAGE_ROW_BANNER + '\');" class="btn btn-primary">         <i class="fa fa-pencil"></i></button>        <button type="button" id="button-clear" class="btn btn-danger" onclick="adminProduct.removeUploadThumb(\'image-thumb' + adminProduct.IMAGE_ROW_BANNER + '\');"><i class="fa fa-trash-o"></i></button>      </div>';
        html += '<input type="hidden" name="banner_image[2][' + adminProduct.IMAGE_ROW_BANNER + '][image]" value="" id="image-thumb' + adminProduct.IMAGE_ROW_BANNER + '_hidden">';
        html += '</td>';
        html += '<td class="text-right" style="width: 10%;"><input type="text" name="banner_image[2][' + adminProduct.IMAGE_ROW_BANNER + '][sort_order]" value="" placeholder="Sắp thứ tự" class="form-control"></td>';
        html += '<td class="text-left"><button type="button" onclick="$(\'#image-row' + adminProduct.IMAGE_ROW_BANNER + ', .tooltip\').remove();" data-toggle="tooltip" title="Loại bỏ" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);

        adminProduct.IMAGE_ROW_BANNER++;
    },
}

// A $( document ).ready() block.
$( document ).ready(function() {
    adminProduct.initUploadBaseOnChangeFile();
});