var dacsandayInit = {
	baseUrl : "https://dacsanday.net",
	homePage : function () {
		$( document ).ready(function() {
		    $(".add-to-cart").click(function(){
		    	var productId = $(this).attr('product-add-to-cart');
		    	var productName = $(this).attr('product-name');
		    	cart.addToCart(productId, productName);
		    });
		});
	},

	detailProductPage : function () {
		$( document ).ready(function() {
		    $(".shopNow").click(function(){
		    	var productId = $(this).attr('product-add-to-cart');
		    	var productName = $(this).attr('product-name');
		    	cart.addToCart(productId, productName);
		    });
		    $(".btn-success").click(function(){
		    	var productId = $(this).attr('product-add-to-cart');
		    	var productName = $(this).attr('product-name');
		    	cart.addToCart(productId, productName);
		    });
		    $(".add-to-cart").click(function(){
		    	var productId = $(this).attr('product-add-to-cart');
		    	var productName = $(this).attr('product-name');
		    	cart.addToCart(productId, productName);
		    });
		});
	},

	initPageCartPage : function (){
		$( document ).ready(function() {
			// Init quantity lost focus
			cart.initCartQuantityKeyUp();
			// Reset list removed product out of cart
			$.setCookie(cart.cookieRemovedName, '',{ expires: 30, path: '/' });
			
			// Hide box righ include cart and like box
			$("#boxCart").addClass("hidden-xs hidden-sm");
		});
	},
	
	initButtonDisplayMenuOnMobile : function () {
	    $( document ).ready(function() {
	        $(".navbar-toggle").click(function() {
    	        var isDisplay = $("#page-menu").is(":visible"); 
        	    if(isDisplay == false) {
        	        $("#page-menu").show(); 
        	    } else {
        	        $("#page-menu").hide(); 
        	    }
	        });
	    });
	}
}

var cart = {
	cookieName : 'cart',
	cookieRemovedName : 'cartremoved',
	maxSubItemInCart : 100,
	feeValueFastMin : 50000,
	feeValueFastMax : 60000,
	feeValueSlowMin : 35000,
	feeValueSlowMax : 40000,
	addToCart : function (productId, productName) {
		// Show box loading
		commonFunction.addLoading();
		// Write data cookie shopping cart
		var cartData = $.getCookie(cart.cookieName);
		if(cartData == null || cartData == undefined) {
			cartData = productId;
		} else {
			cartData = cartData + "_" + productId;
		}
		$.setCookie(cart.cookieName, cartData,{ expires: 30, path: '/' });
		// Load valid product from server
		var url= dacsandayInit.baseUrl + "/gio-hang/getitemincart";
		$.get(url,function(res){
			$("#itemincart").html(res);
			setTimeout(function() {
				commonFunction.removeLoading();
				// Show box alert
				cart.showBoxAlertAddToCart(productName);
			},1000);
		});
	},

	deleteProduct : function (id, order) {
		// Add loading
		commonFunction.addLoading();
		// Remove cookie
		var cartCookie = $.getCookie(cart.cookieName);
		if(cartCookie != null) {
			var strCart="";
			var arrProduct = cartCookie.split("_");
			if(arrProduct.length > 0) {
				for(var i = 0; i< arrProduct.length;i++) {
					if(arrProduct[i] != id ) {
						strCart = strCart + arrProduct[i] + "_";
					}
				}
				strCart = strCart.substring(0,strCart.length-1);
				$.setCookie(cart.cookieName, strCart,{ expires: 30, path: '/' });
				// Set list product was deleted
				cart.addDeletedProductInCookie(id);
			}
		}
		// Remove row product deleted
		$("#pd_"+order).remove();
		// Get total item in cart
		cart.getItemInCart();
		// Recalculating sum sub total
		cart.calSubTotal(order);
		// Recalculationg total money
		cart.calTotalMoney();
		// Set time out to off loading
		setTimeout(function(){
			var totalProductRemain = parseInt($("#itemincart").html());
			if(totalProductRemain > 0) {
				commonFunction.removeLoading();
			} else {
				window.location.href = dacsandayInit.baseUrl + "/gio-hang";
			}
		},1000)
	},

	getItemInCart : function () {
		var url = dacsandayInit.baseUrl + "/gio-hang/getitemincart";
		$.get(url,function(res){
			$("#itemincart").html(res);
		});
	},
	
	showBoxAlertAddToCart : function (productName) {
		var boxAlertAddToCart = "<div id='boxAlertAddToCart'><div id='pro-modal' class='modal fade in' tabindex='-1' role='dialog' style='display: block; padding-right: 10px;'>"
		   +"<div class='modal-dialog'>"
		   +"   <div class='modal-content'>"
		   +"    <div class='modal-header'>"
		   +"       <button type='button' class='close' data-dismiss='modal' aria-label='Close' onclick'cart.removeboxalertaddtocart()'><span aria-hidden='true'>×</span></button>"
		   +"       <h4 class='modal-title'>Thông báo</h4>"
		   +"    </div>"
		   +"    <div class='modal-body'>Thêm sản phẩm <b>"+productName+"</b> vào giỏ hàng thành công.</div>"
		   +"    <div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Mua thêm</button><a href='"+dacsandayInit.baseUrl+"/gio-hang' class='btn btn-primary'>Gởi đơn hàng</a></div>"
		   +" </div>"
		   +"</div>"
		   +"</div>"
		   +"<div class='modal-backdrop fade in'></div></div>";
		$("body").append( boxAlertAddToCart);
		setTimeout(function(){
			$(".close").click(function(){
				$("#boxAlertAddToCart").remove();
			});
			$(".btn-default").click(function(){
				$("#boxAlertAddToCart").remove();
			});
		},1000);
	},

	removeboxalertaddtocart : function () { alert('abc');
		$("#boxAlertAddToCart").hide();
	},

	downQuantity: function (order){
		var currentNumItem = parseInt($("#quantity_"+order).val());
		if(currentNumItem > 1) {
			// Update nuber item product
			currentNumItem = currentNumItem - 1;
			$("#quantity_"+order).val(currentNumItem);
			// Update cookie
			var productId = $("#quantity_"+order).attr('pdid');
			cart.removeProductFromCookie(productId);
			// Recalculating sum sub total
			cart.calSubTotal(order);
			// Recalculationg total money
			cart.calTotalMoney();
		}
	},

	upQuantity: function (order){
		var currentNumItem = parseInt($("#quantity_"+order).val());
		if(currentNumItem < cart.maxSubItemInCart) {
			currentNumItem = currentNumItem + 1;
			$("#quantity_"+order).val(currentNumItem);
			// Update cookie
			var productId = $("#quantity_"+order).attr('pdid');
			cart.addProductToCookie(productId);
			// Recalculating sum sub total
			cart.calSubTotal(order);
			// Recalculationg total money
			cart.calTotalMoney();
		}
	},

	calSubTotal : function (order) {
		var price = parseInt($("#cost_"+order).attr('relcost'));
		var quantity = parseInt($("#quantity_"+order).val());
		var subTotal = price * quantity;
		var strSubTotal = commonFunction.numberWithCommas(subTotal,".");
		$("#subTotal_"+order).html(strSubTotal);
	},

	calTotalMoney : function () {
		var totalProduct = parseInt($("#originTotalProduct").val());
		var totalMondey = 0;
		for(var order=1; order <= totalProduct; order++) {
			if($("#pd_"+order).length > 0) {
				var price = parseInt($("#cost_"+order).attr('relcost'));
				var quantity = parseInt($("#quantity_"+order).val());
				var subTotal = price * quantity;
				totalMondey += subTotal;
			}
		}
		// Tien hang
		var strTotalMoney = commonFunction.numberWithCommas(totalMondey,".");
		$("#totalBill").html(strTotalMoney);

		// Phi ship
		var feeShip = 0;
		var shipArea = parseInt($("#ship_area").val());
	    if(shipArea == 1) {
    	    if($('#delivery_type_fast').prop('checked') == true) {
    	        feeShip = $('#district_area').find(":selected").attr("relfast");
    	    }
    	    if($('#delivery_type_slow').prop('checked') == true) {
    	        feeShip = $('#district_area').find(":selected").attr("relslow");
    	    }
            var strFeeShip = commonFunction.numberWithCommas(feeShip,".");
    		$("#totalShip").html(strFeeShip);

    		// Sum all
    		var sumAll = parseInt(totalMondey) + parseInt(feeShip);
    		var strSumAll = commonFunction.numberWithCommas(sumAll,".");
		    $("#sumAll").html(strSumAll);
	    } else {
            if($('#delivery_type_fast').prop('checked') == true) {
                var strFeeShip = commonFunction.numberWithCommas(cart.feeValueFastMin,".") + " ~ " + commonFunction.numberWithCommas(cart.feeValueFastMax,".");;
    		    $("#totalShip").html(strFeeShip);
    	        // Sum all
        		var sumMin = parseInt(totalMondey) + parseInt(cart.feeValueFastMin);
        		var sumMax = parseInt(totalMondey) + parseInt(cart.feeValueFastMax);
        		var strSumAll = commonFunction.numberWithCommas(sumMax,".");
    		    $("#sumAll").html(strSumAll);
    	    }
    	    if($('#delivery_type_slow').prop('checked') == true) {
                var strFeeShip = commonFunction.numberWithCommas(cart.feeValueSlowMin,".") + " ~ " + commonFunction.numberWithCommas(cart.feeValueSlowMax,".");;
    		    $("#totalShip").html(strFeeShip);
    	        // Sum all
        		var sumMin = parseInt(totalMondey) + parseInt(cart.feeValueSlowMin);
        		var sumMax = parseInt(totalMondey) + parseInt(cart.feeValueSlowMax);
        		var strSumAll = commonFunction.numberWithCommas(sumMax,".");
    		    $("#sumAll").html(strSumAll);
    	    }
	    }
	},

	addProductToCookie : function (productId){
		var cartData = $.getCookie(cart.cookieName);
		if(cartData == null || cartData == undefined) {
			cartData = productId;
		} else {
			cartData = cartData + "_" + productId;
		}
		$.setCookie(cart.cookieName, cartData,{ expires: 30, path: '/' });
	},
	
	removeProductFromCookie : function (productId){
		var cartData = $.getCookie(cart.cookieName);
		if(cartData == null || cartData == undefined) {
			return;
		} else {
			var n = cartData.lastIndexOf(productId);
			if(n > 0){
				if(cartData.substring(n+2).length > 0) {
					cartData = cartData.substring(0,n-1) + "_" + cartData.substring(n+2);
				} else {
					cartData = cartData.substring(0,n-1);
				}
			}
		}
		$.setCookie(cart.cookieName, cartData,{ expires: 30, path: '/' });
	},

	addDeletedProductInCookie : function (productId){
		var cartData = $.getCookie(cart.cookieRemovedName);
		if(cartData == null || cartData == undefined) {
			cartData = productId;
		} else {
			cartData = cartData + "_" + productId;
		}
		$.setCookie(cart.cookieRemovedName, cartData,{ expires: 30, path: '/' });
	},

	getDeletedProductInCookie : function (){
		var cartData = $.getCookie(cart.cookieRemovedName);
		if(cartData == null || cartData == undefined) {
			return 0;
		} else {
			var count = 0;
			var arrIds = cartData.split("_");
			for(var i = 0; i< arrIds.length; i++) {
				if(arrIds.length > 0) {
					count++;
				}
			}
			return count;
		}
	},

	initCartQuantityKeyUp : function () {
		$(".cart-qty").blur(function (){
			var order = $(this).attr("order"); console.info(order);
			var totalSubItem = parseInt($("#quantity_"+order).val());
			if(totalSubItem > cart.maxSubItemInCart) {
				$(this).val(cart.maxSubItemInCart);
			}
			cart.calSubTotal(order);
			cart.calTotalMoney();
		});
	},
	
	sendCart : function () {
		var buyerPhone = $("#buyer_phone").val().trim();
		if(buyerPhone.trim().length == 0) {
		    alert("Số điện thoại người nhận không được để trống");
		    $("#buyer_phone").focus();
		    return false;
		}
		if(buyerPhone.trim().length < 10) {
		    alert("Số điện thoại tối thiểu là 10 số");
		    return false;
		}
		var buyerEmail = $("#buyer_email").val().trim();
		if(buyerEmail.trim() > 0) {
		    var isValidEmail = commonFunction.validateEmail(buyerEmail);
		    if(isValidEmail == false) {
		        $("#buyer_email").focus();
		        alert("Email không hợp lệ");
		        return false;
		    }
		}
		var buyerAddress = $("#buyer_address").val().trim();
		if(buyerAddress.trim().length == 0) {
		    $("#buyer_address").focus();
		    alert("Địa chỉ nhận hàng không được để trống");
		    return false;
		}
		var buyerAddress = $("#buyer_address").val().trim();
		var buyerProvince = $("#buyer_province").val().trim();
		if(buyerProvince.trim().length == 0) {
		    alert("Vui lòng chọn tỉnh/thành phố");
		    return false;
		}
		// Set feeShip to hidden field
		$("#feeShip").val($("#totalShip").html());

		//Show loading
		commonFunction.addLoading();

		// Submit form
		$("#frm_savecart").submit();
	},
	
	changeShipArea : function () {
	    var shipArea = parseInt($("#ship_area").val());
	    if(shipArea == 1) {
	        $("#district_area").show();
	        $("#district_area_label").show();
	        $("#time_delivery_fast").html("Dự kiến giao trước 17h00");
	        $("#time_delivery_slow").html("Dự kiến giao sau 1-2 ngày làm việc");
    	    cart.changeDistrictArea();
	    } else {
	        $("#district_area").hide();
	        $("#district_area_label").hide();
	        $('#delivery_type_fast').removeAttr("disabled");
	        $('#delivery_type_fast').prop('checked', true);
	        $("#fee_value_slow").html(commonFunction.numberWithCommas(cart.feeValueSlowMin,",") + " ~ " + commonFunction.numberWithCommas(cart.feeValueSlowMax,","));
	        $("#fee_value_fast").html(commonFunction.numberWithCommas(cart.feeValueFastMin,",") + " ~ " + commonFunction.numberWithCommas(cart.feeValueFastMax,","));
	        $("#time_delivery_fast").html("Dự kiến giao sau 3-4 ngày làm việc");
	        $("#time_delivery_slow").html("Dự kiến giao sau 4-7 ngày làm việc");
	    }
	    cart.calTotalMoney();
	},
	
	changeDistrictArea : function () {
	    var feeSlow = $('#district_area').find(":selected").attr("relslow");
	    var feeFast = $('#district_area').find(":selected").attr("relfast");
	    // Existed fee fast
	    if(feeFast!="") {
	        $('#delivery_type_fast').removeAttr("disabled");
	        $('#delivery_type_fast').prop('checked', true);
	        $("#fee_value_slow").html(commonFunction.numberWithCommas(feeSlow,","));
	        $("#fee_value_fast").html(commonFunction.numberWithCommas(feeFast,","));
	    } else {
	        $('#delivery_type_fast').prop("disabled","true");
	        $('#delivery_type_slow').prop('checked', true);
	        $("#fee_value_slow").html(commonFunction.numberWithCommas(feeSlow,","));
	        $("#fee_value_fast").html("--");
	    }
	    cart.calTotalMoney();
	}
}

var emailSubcriber = {
    alertEmailSubcriber : function (responseCode) {
        if(responseCode == '-1') {
            alert("Email bạn vừa nhập đã được đăng ký");
        } else if(responseCode == '1') {
            alert("Bạn đã đăng ký email thành công");
        } else if(responseCode == '0') {
            alert("Có lỗi xảy ra vui lòng thử lại sau");
        } else if(responseCode == '999') {
            alert("Email bạn vừa nhập không hợp lệ");
            $("#email").focus();
        }
    }
}

var commonFunction = {
	addLoading : function () {
		var h=$(window).height()/2 - 74;
		var w=$(window).width()/2 - 74;
		var boxLoading = "<div id='boxloading'><div id='p-ovelay' class='p-ovelay' style='position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; z-index: 9999;'></div><div id='p-loading' class='p-loading' style='height: 100%; width: 100%; position: fixed; top: 0px; left: 0px; overflow-y: auto; z-index: 99999;'><div class='inner-loading' style='top: "+h+"px; left: "+w+"px;'></div></div></div>";
		$("body").append( boxLoading);
	},

	removeLoading : function () {
		$("#boxloading").remove();
	},

	numberRemoveCommas : function (x, sDot){
	    return x.toString().replace(sDot, "");
	},

	numberWithCommas : function (x, sDot){
	    x = x.toString().replace(sDot, "");
	    return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, sDot);
	},
	validateEmail : function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}
var api = {
    bx:null,
    bxlogin:null,
    bxsignin:null,
    setimg:function(id){ 
        var descimg=$("#descimg").attr("src");
        var descimgid=$("#descimg_"+id).attr("src");
        $("#descimg").attr("src",descimgid);
        //$('#desca').lightBox();
        api.zoomImg('desca');
    },
     
    search:function(){
        var keyword=$("#keyword").val();
        if(keyword.length==0 || keyword=="Tìm kiếm..."){
            alert("Vui lòng nhập từ khóa tìm kiếm");
            $("#keyword").val("");
            $("#keyword").focus();
        }else{
            window.location=baseUrl+"/product/search?kw="+keyword;
        }
    },
     
    keypresssearch:function(event){
        if(event.keyCode == 13){
            api.search();
        }
    },
     
    checkout:function(){
        $("#frm_order").submit();  
    },
     
    click:function(id){
        var url=baseUrl+"/product/click";
        $.getJSON(url,{
            id:id
        }, function(data){});
    },
     
    showBoxy:function(id){
        
        if(api.bx==null)
        {   
            api.bx= new Boxy(
            
                "<form class='guiloinhan' action='"+baseUrl+"/popup/message' method='POST' target='ifmsg' name='frmmess' id='frmmess'>"+

                "<ul>"+

                "<li>"+

                "<label>Họ tên: <span class='sao'>*</span></label> "+

                "<input id='namekh' type='text' name='namekh' class='ipvl'>"+

                "</li>"+

                "<li>"+

                "<label>Số điện thoại: <span class='sao'>*</span></label> "+

                "<input id='phonekh' type='text' name='phonekh' class='ipvl'>"+

                "</li>"+

                "<li>"+

                "<label>Địa chỉ mail: </label> "+

                "<input id='emailkh' type='text' name='emailkh' class='ipvl'>"+

                "</li>"+

                "<li>"+

                "<label>Nội dung lời nhắn: <span class='sao'>*</span></label> "+

                "<textarea id='ctmsg' style='max-height:186px;max-width:400px;min-heigth:100px;min-width:400px; padding:5px;font-size:12px;font-family:arial,tahoma;' name='ctmsg' onkeyup=\"api.limitLen('ctmsg',200)\"></textarea>"+

                "<p class='demkytu'><span id='msgnotice'>( Tối đa 200 ký tự )</span></p>  "+

                "</li>"+

                "<li id='liprodid'>"+

                "<label>Mã sản phẩm: </label> "+

                "<input id='prodid' type='text' value='408' readonly='readonly' name='prodid' class='ipvl'>"+

                "</li>"+

                "</ul>"+

                "<div style='height:37px;width:100%'><div style='margin:0 auto;height:37px;width:108px'><input type='button' onclick='api.submitmsg();' value='Gửi lời nhắn' class='sbmessage' name='guiloinhan'></div></div>"+

                "</form>"+
                "<iframe name='ifmsg' id='ifmsg' frameborder='no' height='0' width='0' style='display:none;'/>"
                , {
                    title: "Để lại lời nhắn",
                    modal:"true",
                    closeText:"[X]"
                });

        }
        $("#namekh").val('');
        $("#phonekh").val('');
        $("#emailkh").val('');
        $("#ctmsg").val('');
        $("#namekh").val('');
        $("#namekh").val('');
        
        if(id>0)
            $('#prodid').val(  $('#idprodmsg').val() );
        else{
            $('#prodid').val(id);
            $('#liprodid').css('display','none');
            
        }
        $('#msgnotice').html('( Tối đa 200 ký tự )');  
        api.bx.show();
        api.bx.centerAt($(window).width()/2,$(window).height()/2);
        $("#namekh").focus();
        
        setTimeout(function(){
            $("#namekh").keypress(function(event){
                if(event.keyCode == 13){
                    $("#phonekh").focus();
                }
            });
            $("#phonekh").keypress(function(event){
                if(event.keyCode == 13){
                    $("#emailkh").focus();
                }
            });
            $("#emailkh").keypress(function(event){
                if(event.keyCode == 13){
                    $("#ctmsg").focus();
                }
            });
            
        },1000);
    },
    
    addcard:function(){
        var id=$("#idprod").val();
        var qty=$("#qty").val();
        var info=id+"-"+qty;
        var prod=$.cookie(selprod);
        if(prod!=undefined){            
            prod=$.base64.decode(prod);
            prod+=","+info;
        }
        else{
            prod=info;
        }        
        prod=$.base64.encode(prod);        
        $.cookie(selprod,prod,{
            expires:30
        });
        window.location=baseUrl+"/product/card";
    },
    
    removecard:function(id){   
        var prod=$.cookie(selprod);
        var data="";
        if(prod!=undefined){            
            prod=$.base64.decode(prod);
            var arr=prod.split(',');
            var n=arr.length;
            for(var i=0;i<n;i++){
                var tmp=arr[i].split('-'); 
                if(tmp[0]!=id){
                    var t=tmp[0]+"-"+tmp[1];
                    data+=tmp[0]+"-"+tmp[1]+",";
                   
                }
            }
            data=data.substr(data,data.length-1); 
            data=$.base64.encode(data);
            $.cookie(selprod,data,{
                expires:30
            });
            window.location=baseUrl+"/product/card";
        }
    },
    
    updatecard:function(){   
        var prod=$.cookie(selprod);
        var data="";
        if(prod!=undefined){            
            prod=$.base64.decode(prod);
            var arr=prod.split(',');
            var n=arr.length;
            for(var i=0;i<n;i++){
                var tmp=arr[i].split('-'); 
                if(tmp[0]!=id){
                    var t=tmp[0]+"-"+tmp[1];
                    data+=tmp[0]+"-"+tmp[1]+",";
                   
                }
            }
            data=data.substr(data,data.length-1); 
            data=$.base64.encode(data);
            $.cookie(selprod,data,{
                expires:30
            });
            window.location=baseUrl+"/product/card";
        }
    },
    
    removeallcard:function(){   
        //msg check is ok
        $.cookie(selprod,"",{
            expires:-1
        });
        window.location=baseUrl;        
    },
    
    smcheckout:function(){   
        var namekh=$("#namekh").val();
        if(namekh=="" || namekh.length==0){
            alert("Vui lòng nhập tên");
            $("#namekh").focus();
            return;
        }
        var phonekh=$("#phonekh").val();
        if(phonekh=="" || phonekh.length==0){
            alert("Vui lòng nhập số điện thoại");
            $("#phonekh").focus();
            return;
        }
        var emailkh=$("#emailkh").val();
        if(emailkh=="" || emailkh.length==0 || emailkh.indexOf("@")==-1){
            alert("Email không hợp lệ");
            $("#emailkh").focus();
            return;
        }
        var addresskh=$("#addresskh").val();
        if(addresskh=="" || addresskh.length==0){
            alert("Vui lòng nhập địa chỉ");
            $("#addresskh").focus();
            return;
        }
        
        var tinh=$("#tinh").val();
        if(tinh<0){
            alert("Vui lòng chọn tỉnh/thành phố");
            $("#tinh").focus();
            return;
        }
        
        var verify=$("#txtCode").val();
        var hdverify=$("#hdverifycode").val();
        if(verify.length==0 || hdverify!=verify){
            $("#txtCode").focus();
            alert('Mã xác nhận không đúng');
            return;
        }
        $.cookie("flgcheckout","1");
        $("#order_frm").submit();
    },
    
    submitmsg:function(){
        var namekh=$("#namekh").val();
        if(namekh=="" || namekh.length==0)
        {       
            api.showalert('Vui lòng nhập tên');
            $("#namekh").focus();
            return;
        }
        
        var phonekh=$("#phonekh").val();
        if(phonekh=="" || phonekh.length==0)
        {       
            api.showalert('Vui lòng nhập điện thoại');
            $("#phonekh").focus();
            return;
        }
        
        var ctmsg=$("#ctmsg").val();
        if(ctmsg=="" || ctmsg.length==0)
        {       
            api.showalert('Vui lòng nhập nội dung tin nhắn');
            $("#ctmsg").focus();
            return;
        }
        
        var emailkh=$("#emailkh").val();
        if(emailkh.length>0 && emailkh.indexOf("@", 0)==-1)
        {       
            api.showalert('Email không hợp lệ');
            $("#emailkh").focus();
            return;
        }
        
        $("#frmmess").submit();
        api.bx.hide();
    },
    
    showalert:function(msg){
        alert(msg);
    },
    
    sumcard:function(id){
        var url=baseUrl+"/product/sumcard?callback=?";
        $.getJSON(url, function(res){
            $("#sumcard").html(res.total);
        });
    },
    
    limitLen:function(id){
        var MAX_LENGH=200;
        var content =$('#'+id).val();
        if(content.length> MAX_LENGH)
        {
            content=content.substr(0,MAX_LENGH);
            $('#'+id).val(content);

        }
        $('#msgnotice').html('( Tối đa ' +(MAX_LENGH - content.length)+ ' ký tự )');
    },
    
    login:function(){
        
        if(api.bxlogin==null)
        {   
            api.bxlogin= new Boxy(
                "<form class='guiloinhan' action='"+baseUrl+"/auth/login' method='POST' target='iflogin' name='frmlogin' id='frmlogin'>"+
                "<div id='errMsg' style='display: block; font-size: 12px; text-align: center; color: red;padding-bottom:20px'></div>"+
                "<ul>"+
                "<li style='padding-bottom: 8px;'>"+
                "<label style='width:120px'>Tên đăng nhập: <span class='sao'>*</span></label> "+
                "<input style='width:250px' id='txtacc' type='text' name='txtacc' class='ipvl'>"+
                "</li>"+
                "<li>"+
                "<label style='width:120px'>Mật khẩu: <span class='sao'>*</span></label> "+
                "<input style='width:250px' id='txtpass' type='password' name='txtpass' class='ipvl'>"+
                "</li>"+
                "</ul>"+
                "<div class='oplogin2' style='font-size:12px;padding-top:10px'><p class='f12'>Chưa có tài khoản, <a href='"+baseUrl+"/auth/signin' class='blue'>Đăng ký | </a><a href='"+baseUrl+"/auth/recoverpass' class='blue'>Quên mật khẩu?</a></p></div>"+
                "<div style='height:37px;width:100%;padding-top:20px'><div style='margin:0 auto;height:37px;width:108px'><input type='button' onclick='api.submitlogin();' value='Đăng nhập' class='sbmessage' name='login'></div></div>"+
                "</form>"+
                "<iframe frameborder='0' height='0' name='iflogin' id='iflogin' frameborder='no' height='0' width='0' style='display:none;' height='0px'/>"
                , {
                    title: "Đăng nhập",
                    modal:"true",
                    closeText:"[X]"
                });

        }
        $("#txtacc").val('');
        $("#txtpass").val('');
        $('#errMsg').html('');
        api.bxlogin.show();
        api.bxlogin.centerAt($(window).width()/2,$(window).height()/2);
        $("#txtacc").focus();
        
        setTimeout(function(){
            $("#txtacc").keypress(function(event){
                if(event.keyCode == 13){
                    $("#txtpass").focus();
                }
            });
            $("#txtpass").keypress(function(event){
                if(event.keyCode == 13){
                    api.submitlogin();
                }
            });
        },1000);
       
    },
    
    submitlogin:function(){
        var txtacc=$("#txtacc").val();
        if(txtacc=="" || txtacc.length==0)
        {       
            api.showalert('Tên đăng nhập không được bỏ trống');
            $("#txtacc").focus();
            return;
        }        
        var txtpass=$("#txtpass").val();
        if(txtpass=="" || txtpass.length==0)
        {       
            api.showalert('Mật khẩu không được bỏ trống');
            $("#txtpass").focus();
            return;
        }        
        $("#frmlogin").submit();
    //api.bxlogin.hide();
    },
    
    signin:function(){
        
        if(api.bxsignin==null)
        {   
            api.bxsignin= new Boxy(
                "<form class='guiloinhan' action='"+baseUrl+"/auth/login' method='POST' target='iflogin' name='frmlogin' id='frmlogin'>"+
                "<div id='errMsg' style='display: block; font-size: 12px; text-align: center; color: red;padding-bottom:20px'></div>"+
                "<ul>"+
                "<li style='padding-bottom: 8px;'>"+
                "<label style='width:120px'>Tên đăng nhập: <span class='sao'>*</span></label> "+
                "<input style='width:250px' id='txtacc' type='text' name='txtacc' class='ipvl'>"+
                "</li>"+
                "<li>"+
                "<label style='width:120px'>Mật khẩu: <span class='sao'>*</span></label> "+
                "<input style='width:250px' id='txtpass' type='password' name='txtpass' class='ipvl'>"+
                "</li>"+
                "</ul>"+
                "<div style='height:37px;width:100%;padding-top:20px'><div style='margin:0 auto;height:37px;width:108px'><input type='button' onclick='api.submitlogin();' value='Đăng nhập' class='sbmessage' name='login'></div></div>"+
                "</form>"+
                "<iframe frameborder='0' height='0' name='iflogin' id='iflogin' frameborder='no' height='0' width='0' style='display:none;' height='0px'/>"
                , {
                    title: "Đăng ký tài khoản",
                    modal:"true",
                    closeText:"[X]"
                });

        }
        $("#txtacc").val('');
        $("#txtpass").val('');
        $('#errMsg').html('');
        api.bxsignin.show();
        api.bxsignin.centerAt($(window).width()/2,$(window).height()/2);
        $("#txtacc").focus();
        
        setTimeout(function(){
            $("#txtacc").keypress(function(event){
                if(event.keyCode == 13){
                    $("#txtpass").focus();
                }
            });
            $("#txtpass").keypress(function(event){
                if(event.keyCode == 13){
                    api.submitlogin();
                }
            });
        },1000);
       
    },
    
    zoomImg:function(id){
        var options = {  
            zoomType: 'standard',  
            lens:true,  
            preloadImages: true,  
            alwaysOn:false,
            zoomWidth: 450,  
            zoomHeight: 450, 
            xOffset:30,  
            yOffset:0,  
            position:'right'  
        };  
        $('#'+id).jqzoom(options); 
    },
    
    showMenu:function(count){
        
        $("#mnu_0").hover(function(){ 
                $("#sub_0").css("display","block");
            },function(){
                $("#sub_0").css("display","none");
            }); 
            
             $("#mnu_1").hover(function(){ 
                $("#sub_1").css("display","block");
            },function(){
                $("#sub_1").css("display","none");
            });
            
             $("#mnu_2").hover(function(){ 
                $("#sub_2").css("display","block");
            },function(){
                $("#sub_2").css("display","none");
            });
             $("#mnu_3").hover(function(){ 
                $("#sub_3").css("display","block");
            },function(){
                $("#sub_3").css("display","none");
            });
            
//        
//        
//        
//        for(var i=0;i<count;i++){
//            $("#mnu_"+i).hover(function(){ console.info("hover: "+ i );
//                $("#sub_"+i).css("display","block");
//            },function(){console.info("leave: "+i);
//                $("#sub_"+i).css("display","none");
//            }); 
//        }
    }
}


