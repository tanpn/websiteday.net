var api = {
     showDetailInfo:function(type){
        if(type=='fcond'){
            $("#fdesc").css("display", "none");
            $("#fconf").css("display", "block");
            $("#tconf").addClass("active");
            $("#tdesc").removeClass("active");
        }else{
             $("#fdesc").css("display", "block");
            $("#fconf").css("display", "none");
             $("#tdesc").addClass("active");
            $("#tconf").removeClass("active");
        }
        
     },
     
     search:function(){
         var keyword=$("#keyword").val();
         if(keyword.length==0 || keyword=="Tìm kiếm..."){
             alert("Vui lòng nhập từ khóa tìm kiếm");
             $("#keyword").val("");
             $("#keyword").focus();
         }else{
             window.location=baseUrl+"/product/search?kw="+keyword;
         }
     },
     
     keypresssearch:function(event){
         if(event.keyCode == 13){
            api.search();
        }
     },
     
     checkout:function(){
       $("#frm_order").submit();  
     },
     
     click:function(id){
         var url=baseUrl+"/product/click";
         $.getJSON(url,{id:id}, function(data){});
     },
     
     showBoxy:function(){
        
        if(api.bx==null)
        {         
            api.bx= new Boxy(
                "<div class='zme-boxy-body'><div class='zme-boxy-content'><div class='feedback-form'>"+
                "<div class='rowFB'>"+
                "<span class='coltext'><span class='abbr'>*</span> Chủ đề:</span> <span class='coltype'>"+
                "<select id='fbSubject'>"+
                "<option value=''>Vui lòng chọn chủ đề cần phản hồi</option>"+
                "<option value='1'>Báo lỗi</option>"+
                "<option value='2'>Góp ý nội dung</option>"+
                "<option value='3'>Yêu cầu chức năng</option>"+
                "<option value='4'>Khác</option>"+
                "</select>"+
                "</span> <div class='clr'></div>"+
                "</div>"+
                "<div class='rowFB'>"+
                "<span class='coltext'>Link:</span> <span class='coltype'>"+
                "<input type='text' maxlength='100' id='linkid' value='"+location.href+"' class='input'></span>"+
                "<div class='clr'></div>"+
                "</div>"+
                "<div class='rowFB'>"+
                "<span class='coltext'><span class='abbr'>*</span> Nội dung:</span>"+
                "<span class='coltype'><textarea id='content' rows='5' cols='' style='height: 97px;'></textarea></span>"+
                "<div class='clr'></div>"+
                "</div>"+
                "</div>"+
                "<input type='hidden' name='is_post' value='true'>"+
                "</div><div class='zme-boxy-footer'><a class='btn_L3' tabindex='0' href='javascript:void(0);' onclick='GHApi.hideBoxy();'><em>Gởi</em></a><a onclick='Boxy.get(this).hide(); return false' class='btn_L5' tabindex='0' href='#'><em>Đóng</em></a></div></div>"+
                "", {
                    title: "Góp ý / Báo lỗi",
                    modal:"true",
                    closeText:"[X]"
                });

        }else
        {
            $('#linkid').val(location.href);
            $('#content').val('');
            var sel = document.getElementById("fbSubject");
            sel.selectedIndex=0;
            api.bx.show();
            api.bx.centerAt($(window).width()/2,$(window).height()/2);
        }
       
    },
    
    addcard:function(){
        var id=$("#idprod").val();
        var qty=$("#qty").val();
        var info=id+"-"+qty;
        var prod=$.cookie(selprod);
        if(prod!=undefined){            
            prod=$.base64.decode(prod);
            prod+=","+info;
        }
        else{
            prod=info;
        }        
        prod=$.base64.encode(prod);        
        $.cookie(selprod,prod);
        window.location=baseUrl+"/product/card";
    },
    
    removecard:function(id){   
        $("#pro_"+id).css("display","none");
//        var prod=$.cookie(selprod);
//        if(prod!=undefined){            
//            prod=$.base64.decode(prod);
//            var arr=data.split(',');
//            
//        }
//        
//        if(dt!=undefined){
//            var data=$.base64.decode(dt);
//            var arr=data.split('-');
//            str=id+"-";
//            var pos=1;
//            for(var i=0;i<arr.length;i++){
//                var item=arr[i];
//                if(item.length>0){ 
//                    if(parseInt(item)!=parseInt(id)) {
//                        if(pos<MAX_ITEM_COOKIE_PLAYED )
//                            str+=item+"-";
//                        else
//                            break;
//                        pos++;
//                    }
//                }
//            } 
//        }else {
//            str=id;
//        } 
//        str=$.base64.encode(str);                 
//        $.cookie('playlist', str, {
//            expires:999999
//        });
//
//        prod=$.base64.encode(prod);        
//        $.cookie(selprod,prod);
//        window.location=baseUrl+"/product/card";
    }
}


